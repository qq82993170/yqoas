/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : business

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-04-02 11:04:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户组';

-- ----------------------------
-- Records of auth_group
-- ----------------------------
INSERT INTO `auth_group` VALUES ('1', '超级管理员', '1', '1,2,5,4,6,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30');
INSERT INTO `auth_group` VALUES ('2', '管理员', '1', '59,99,104,101,103,122,121,109,120,102,119,100,105,107,123,106,108,115,116,117,118,24,31,32,86,85,92,33,82,83,34,110,111,113,112,35,98,36,27,26,25,28,29,30,56,57,39,40,41,96,97,42,87,88,43,90,89,47,38,51,48,114,49,93,94,95,91,53,54,55,18,19,20,21,22,23,5,6,1,7,8,9,2,124,11,3,12,14,13,4,15,16,58,17');

-- ----------------------------
-- Table structure for auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_access`;
CREATE TABLE `auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限配置';

-- ----------------------------
-- Records of auth_group_access
-- ----------------------------
INSERT INTO `auth_group_access` VALUES ('1', '1');
INSERT INTO `auth_group_access` VALUES ('2', '1');
INSERT INTO `auth_group_access` VALUES ('3', '1');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `islink` tinyint(1) NOT NULL DEFAULT '1',
  `o` int(11) NOT NULL COMMENT '排序',
  `tips` text NOT NULL,
  `mold` smallint(6) DEFAULT '1' COMMENT '导航类型：1导航栏 2导航栏右侧菜单',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COMMENT='后台菜单管理';

-- ----------------------------
-- Records of auth_rule
-- ----------------------------
INSERT INTO `auth_rule` VALUES ('2', '5', '', '考勤管理', '', '1', '1', '', '1', '2', '', '1');
INSERT INTO `auth_rule` VALUES ('3', '5', '', '部门管理', '', '1', '1', '', '1', '3', '', '1');
INSERT INTO `auth_rule` VALUES ('4', '5', '', '系统设置', '', '1', '1', '', '1', '4', '', '1');
INSERT INTO `auth_rule` VALUES ('5', '0', '', '顶部导航菜单', '', '1', '1', '', '0', '2', '', '1');
INSERT INTO `auth_rule` VALUES ('6', '5', 'Index/index', ' [ 控制台 ]', '', '1', '1', '', '1', '0', '', '1');
INSERT INTO `auth_rule` VALUES ('7', '1', 'Work/index', '工作记录', '', '1', '1', '', '1', '0', '', '1');
INSERT INTO `auth_rule` VALUES ('8', '1', 'Work/add', '工作提交', '', '1', '1', '', '1', '1', '', '1');
INSERT INTO `auth_rule` VALUES ('9', '1', 'Monthly/add', '月报提交', '', '1', '1', '', '1', '2', '', '1');
INSERT INTO `auth_rule` VALUES ('11', '2', 'Attence/index', '绩效考评', '', '1', '1', '', '1', '1', '', '1');
INSERT INTO `auth_rule` VALUES ('12', '3', 'Member/index', '用户管理', '', '1', '1', '', '1', '0', '', '1');
INSERT INTO `auth_rule` VALUES ('13', '3', 'News/index', '动态管理', '', '1', '1', '', '1', '2', '', '1');
INSERT INTO `auth_rule` VALUES ('14', '3', 'Files/index', '文件管理', '', '1', '1', '', '1', '1', '', '1');
INSERT INTO `auth_rule` VALUES ('15', '4', 'Update/index', '更新日志', '', '1', '1', '', '1', '0', '', '1');
INSERT INTO `auth_rule` VALUES ('16', '4', 'Setting/index', '网站设置', '', '1', '1', '', '1', '1', '', '1');
INSERT INTO `auth_rule` VALUES ('17', '4', 'Group/index', '用户组', '', '1', '1', '', '1', '3', '', '1');
INSERT INTO `auth_rule` VALUES ('23', '18', 'Shares/index', '我的分享', '', '1', '1', '', '1', '4', '', '2');
INSERT INTO `auth_rule` VALUES ('22', '18', 'Dzone/files', '我的文件', '', '1', '1', '', '1', '3', '', '2');
INSERT INTO `auth_rule` VALUES ('24', '59', '', '部门管理', '', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('25', '26', 'Menu/index', '菜单管理', 'fa-navicon', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('26', '27', '', '系统设置', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('27', '59', '', '系统配置', '', '1', '1', '', '1', '3', '', '3');
INSERT INTO `auth_rule` VALUES ('28', '26', 'Setting/index', '网站设置', 'fa-cog', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('29', '27', '', '用户配置', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('30', '29', 'Group/index', '用户组', 'fa-users', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('31', '24', '', '成员管理', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('32', '31', 'Member/index', '用户列表', 'fa-user', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('33', '31', 'Department/index', '部门管理', 'fa-users', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('34', '24', '', '内容管理', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('35', '34', 'News/index', '动态管理', 'fa-commenting', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('36', '34', 'Files/index', '文件管理', 'fa-inbox', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('38', '47', 'Shares/index', '分享管理', 'fa-share-alt-square', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('39', '59', '', '个人中心', '', '1', '1', '', '1', '4', '', '3');
INSERT INTO `auth_rule` VALUES ('40', '39', '', '常用功能', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('41', '40', 'Notes/index', '待办管理', 'fa-file-o', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('42', '40', 'Words/index', '笔记管理', 'fa-file-text', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('43', '40', 'Collection/index', '收藏夹', 'fa-bookmark-o', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('98', '35', 'news/view', '动态查看', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('47', '39', '####', '其他功能', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('48', '47', 'Dzone/department', '部门成员', 'fa-users', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('49', '47', 'Customer/index', '我的客户', 'fa-user-plus', '1', '1', '', '1', '4', '', '3');
INSERT INTO `auth_rule` VALUES ('51', '47', 'Dzone/files', '我的文件', 'fa-inbox', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('53', '39', '', '个人信息', '', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('54', '53', 'Dzone/personal', '修改资料', 'fa-user', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('55', '53', 'Index/logout', '安全退出', 'fa-sign-out', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('1', '5', '', '工作管理', '', '1', '1', '', '1', '1', '', '1');
INSERT INTO `auth_rule` VALUES ('56', '30', 'Group/add', '添加用户组', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('57', '30', 'Group/edit', '编辑用户组', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('58', '4', 'Update/update', '日志添加', '', '1', '1', '', '0', '2', '', '1');
INSERT INTO `auth_rule` VALUES ('21', '18', 'Words/index', '我的笔记', '', '1', '1', '', '1', '2', '', '2');
INSERT INTO `auth_rule` VALUES ('20', '18', 'Notes/index', '我的待办', '', '1', '1', '', '1', '1', '', '2');
INSERT INTO `auth_rule` VALUES ('19', '18', 'Dzone/index', '我的首页', '', '1', '1', '', '1', '0', '', '2');
INSERT INTO `auth_rule` VALUES ('18', '0', '', '顶部右侧菜单', '', '1', '1', '', '0', '1', '', '2');
INSERT INTO `auth_rule` VALUES ('82', '33', 'Department/add', '部门添加', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('59', '0', '', '页面左侧菜单', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('83', '33', 'Department/edit', '部门编辑', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('85', '32', 'Member/add', '添加成员', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('86', '32', 'Member/edit', '编辑成员', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('87', '42', 'Words/add', '添加笔记', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('88', '42', 'Words/edit', '编辑笔记', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('89', '43', 'Collection/add', '添加收藏', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('90', '43', 'Collection/edit', '编辑收藏', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('91', '47', 'Index/logs', '操作信息', 'fa-file-text', '1', '1', '', '1', '5', '', '3');
INSERT INTO `auth_rule` VALUES ('92', '31', 'Member/logs', '用户日志', 'fa-file-text', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('93', '49', 'Customer/add', '客户录入', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('94', '49', 'Customer/add', '客户复录', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('95', '49', 'Customer/view', '客户信息', '', '1', '1', '', '0', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('96', '41', 'Notes/add', '添加待办', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('97', '41', 'Notes/edit', '编辑待办', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('99', '59', '###', '工作管理', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('100', '59', '###', '绩效考勤', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('101', '104', 'Work/add', '工作提交', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('102', '109', 'Work/index', '工作记录', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('103', '104', 'Monthly/add', '月报提交', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('119', '102', 'Work/edit', '编辑此项', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('104', '99', '####', '报 表', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('105', '100', '####', '审 核', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('106', '100', '####', '统 计', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('110', '34', 'Inform/index', '公司通告', 'fa-volume-up', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('107', '105', 'Attence/index', '绩效考评', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('108', '106', 'Attence/works', '工作统计', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('115', '100', '#####', '管 理', '', '1', '1', '', '1', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('109', '99', '####', '记 录', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('111', '110', 'Inform/add', '添加公告', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('112', '110', 'Inform/add', '编辑公告', '', '1', '1', '', '0', '2', '', '3');
INSERT INTO `auth_rule` VALUES ('113', '110', 'Inform/view', '通告查看', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('114', '47', 'Index/inform', '阅读通告', '', '1', '1', '', '0', '3', '', '3');
INSERT INTO `auth_rule` VALUES ('116', '115', 'Reports/index', '报表类型', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('117', '116', 'Reports/add', '增加类型', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('118', '116', 'Reports/edit', '编辑类型', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('120', '109', 'Monthly/index', '月报列表', '', '1', '1', '', '1', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('121', '103', 'Monthly/update', '月报修改保存权限', '', '1', '1', '', '0', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('122', '103', 'Monthly/edit', '月报查看修改界面浏览权限', '', '1', '1', '', '0', '0', '', '3');
INSERT INTO `auth_rule` VALUES ('123', '105', 'Attence/reports', '工作报表', '', '1', '1', '', '1', '1', '', '3');
INSERT INTO `auth_rule` VALUES ('124', '2', 'Attence/reports', '工作报表', '', '1', '1', '', '1', '0', '', '1');

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `uid` int(11) DEFAULT NULL,
  `time` int(10) unsigned DEFAULT NULL,
  `status` smallint(1) unsigned DEFAULT '0' COMMENT '收藏加状态：0、未加星，1、加星',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='收藏夹表';

-- ----------------------------
-- Records of collection
-- ----------------------------

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(500) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL COMMENT '发送人',
  `type` smallint(1) DEFAULT NULL COMMENT '1文章评论2动态评论3聊天信息',
  `be` int(11) unsigned DEFAULT NULL COMMENT '属于谁的',
  `accepter` int(11) unsigned DEFAULT NULL COMMENT '接受者',
  `time` int(10) unsigned DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天表';

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `phone` varchar(222) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hightech` varchar(255) DEFAULT NULL COMMENT '公司',
  `uid` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `stutas` smallint(1) unsigned DEFAULT NULL COMMENT '0:未验证 、1:已验证、2:已失效',
  `time` int(10) DEFAULT NULL COMMENT '录入时间',
  `sex` smallint(1) DEFAULT NULL COMMENT '性别  0：保密、1：帅哥、2：美女',
  `type` smallint(1) DEFAULT NULL COMMENT '1:CEO、2：总监，3：经理、0：普通员工',
  `head` varchar(255) DEFAULT NULL,
  `qq` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='客户表';

-- ----------------------------
-- Records of customer
-- ----------------------------

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `content` text COMMENT '角色数据',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '原创源码库Team', '0', '1478138451', '');
INSERT INTO `department` VALUES ('2', '有穹Team', '0', '1478138414', '');
INSERT INTO `department` VALUES ('3', '技术部', '2', '1478138422', '');
INSERT INTO `department` VALUES ('4', '反馈部', '1', '1478138430', '');
INSERT INTO `department` VALUES ('5', '客服部', '1', '1478138564', '');
INSERT INTO `department` VALUES ('6', '销售部', '1', '1478138697', '');
INSERT INTO `department` VALUES ('7', '对外合作部', '1', '1478138719', '');
INSERT INTO `department` VALUES ('8', '公共事务部', '1', '1478138755', '');
INSERT INTO `department` VALUES ('9', '分析研究部', '1', '1478138764', '');

-- ----------------------------
-- Table structure for document
-- ----------------------------
DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `fid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) DEFAULT NULL,
  `type` varchar(80) DEFAULT NULL COMMENT '1文件,2图片',
  `uid` int(11) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';

-- ----------------------------
-- Records of document
-- ----------------------------

-- ----------------------------
-- Table structure for dynamic
-- ----------------------------
DROP TABLE IF EXISTS `dynamic`;
CREATE TABLE `dynamic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `content` text,
  `time` int(10) DEFAULT NULL,
  `source` smallint(1) DEFAULT NULL COMMENT '动态类型：1、公司动态、2、分享动态、3、我的说说',
  `prez` int(10) unsigned DEFAULT '0' COMMENT '赞的数量',
  `pin` int(6) DEFAULT NULL COMMENT '加密密码',
  `type` smallint(1) unsigned DEFAULT NULL COMMENT '1.文章类型，2.文件类型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='动态表';

-- ----------------------------
-- Records of dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for inform
-- ----------------------------
DROP TABLE IF EXISTS `inform`;
CREATE TABLE `inform` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `start` int(10) DEFAULT NULL,
  `end` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='通告表';

-- ----------------------------
-- Records of inform
-- ----------------------------

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `t` int(10) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `log` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='日志';

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('1', 'admin', '1482031019', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('2', 'admin', '1482031541', '127.0.0.1', '登录失败。');
INSERT INTO `log` VALUES ('3', 'admin', '1482031543', '127.0.0.1', '登录失败。');
INSERT INTO `log` VALUES ('4', 'admin', '1482031545', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('5', 'admin', '1482035145', '127.0.0.1', '登录失败。');
INSERT INTO `log` VALUES ('6', 'admin', '1482035149', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('7', 'admin', '1482035395', '127.0.0.1', '新增文件，ID：1');
INSERT INTO `log` VALUES ('8', 'admin', '1482035396', '127.0.0.1', '新增文件，ID：2');
INSERT INTO `log` VALUES ('9', 'admin', '1482035396', '127.0.0.1', '新增文件，ID：3');
INSERT INTO `log` VALUES ('10', 'admin', '1482035406', '127.0.0.1', '新增文件，ID：4');
INSERT INTO `log` VALUES ('11', 'admin', '1482035406', '127.0.0.1', '新增文件，ID：5');
INSERT INTO `log` VALUES ('12', 'admin', '1482035407', '127.0.0.1', '新增文件，ID：6');
INSERT INTO `log` VALUES ('13', 'admin', '1482035420', '127.0.0.1', '删除文件，ID：2');
INSERT INTO `log` VALUES ('14', 'admin', '1482035423', '127.0.0.1', '删除文件，ID：4');
INSERT INTO `log` VALUES ('15', 'admin', '1482035426', '127.0.0.1', '删除文件，ID：5');
INSERT INTO `log` VALUES ('16', 'admin', '1482035428', '127.0.0.1', '删除文件，ID：6');
INSERT INTO `log` VALUES ('17', 'admin', '1482035430', '127.0.0.1', '删除文件，ID：3');
INSERT INTO `log` VALUES ('18', 'admin', '1482035433', '127.0.0.1', '删除文件，ID：1');
INSERT INTO `log` VALUES ('19', 'admin', '1482545060', '127.0.0.1', '登录失败。');
INSERT INTO `log` VALUES ('20', 'admin', '1482545063', '127.0.0.1', '登录失败。');
INSERT INTO `log` VALUES ('21', 'admin', '1482545072', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('22', 'admin', '1482557800', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('23', 'admin', '1483113808', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('24', 'admin', '1483161408', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('25', 'admin', '1483255059', '127.0.0.1', '登录成功。');
INSERT INTO `log` VALUES ('26', 'admin', '1486555314', '127.0.0.1', '登录失败。');
INSERT INTO `log` VALUES ('27', 'admin', '1486555318', '127.0.0.1', '登录成功。');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(225) NOT NULL,
  `head` varchar(255) NOT NULL COMMENT '头像',
  `sex` tinyint(1) NOT NULL COMMENT '0保密1男，2女',
  `birthday` int(10) NOT NULL COMMENT '生日',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `qq` varchar(20) NOT NULL COMMENT 'QQ',
  `email` varchar(255) NOT NULL COMMENT '邮箱',
  `password` varchar(32) NOT NULL,
  `t` int(10) unsigned NOT NULL COMMENT '注册时间',
  `identifier` varchar(32) NOT NULL,
  `token` varchar(32) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `dept_id` int(5) unsigned DEFAULT NULL COMMENT '部门关联id',
  `tel` int(11) DEFAULT NULL COMMENT '分机号，电话号码',
  `rank` varchar(255) DEFAULT NULL COMMENT '头衔',
  `type` tinyint(1) DEFAULT '0' COMMENT '1:CEO、2：总监，3：经理、0：普通员工',
  `created` int(10) DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='后台用户';

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', 'admin', '/Uploads/thumb/201612/1481902376.png', '1', '1422374400', '15151965305', '861152525', '861157525@qq.com', '66d6a1c8748025462128dc75bf5ae8d1', '1486555318', '24879296a92c551a8dd384fb5fb74391', 'b8660351e92690bf5887c76a96778d16', '8XxcHA2jgx', '管理员', '1', '11212', '11212', '3', '1480153258');
INSERT INTO `member` VALUES ('2', 'user1', 'public/static/img/a9.jpg', '0', '1479139200', '15858985808', '861157525', '861157525@qq.com', '66d6a1c8748025462128dc75bf5ae8d1', '1481957209', '85d30928fc55cb96ce30112992234236', '667f8daee80455eca1f8923c9ca6379d', 'Cf3pZ7pxyd', '用户1', '1', '0', '营销总监', '2', '1480153258');
INSERT INTO `member` VALUES ('3', 'user2', 'public/static/img/a8.jpg', '1', '1479916800', '15858985808', '', '861157525@qq.com', '66d6a1c8748025462128dc75bf5ae8d1', '1481965484', '1dba027d9401c0a5d4b63482397b3d83', '127392bfe5db82dbdb72ec5b7a2f4632', 'dgDtMtQMyV', '用户2', '2', '0', '开发人员', '0', '1480153390');

-- ----------------------------
-- Table structure for monthly
-- ----------------------------
DROP TABLE IF EXISTS `monthly`;
CREATE TABLE `monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) DEFAULT NULL COMMENT '日期',
  `my_assess` text COMMENT '完成情况',
  `manager_assess` text COMMENT '管理工作',
  `my_duty` text COMMENT '工作责任',
  `my_active` text COMMENT '主动积极性',
  `my_observe` text COMMENT '公司制度遵守情况',
  `manager_reason` text COMMENT '经理评分理由',
  `inspector_reason` text COMMENT '总监评分理由',
  `my_assess_score` smallint(3) DEFAULT NULL COMMENT '自我评价的工作得分',
  `my_duty_score` smallint(3) DEFAULT NULL COMMENT '自我评价',
  `my_active_score` smallint(3) DEFAULT NULL COMMENT '自我评价',
  `my_observe_score` smallint(3) DEFAULT NULL COMMENT '自我评价',
  `manager_assess_score` smallint(3) DEFAULT NULL COMMENT '经理评分',
  `manager_duty_score` smallint(3) DEFAULT NULL COMMENT '经理评分',
  `manager_active_score` smallint(3) DEFAULT NULL COMMENT '经理评分',
  `manager_observe_score` smallint(3) DEFAULT NULL COMMENT '经理评分',
  `inspector_assess_score` smallint(3) DEFAULT NULL COMMENT '总监评分',
  `inspector_duty_score` smallint(3) DEFAULT NULL COMMENT '总监评分',
  `inspector_active_score` smallint(3) DEFAULT NULL COMMENT '总监评分',
  `inspector_observe_score` smallint(3) DEFAULT NULL COMMENT '总监评分',
  `uid` int(11) DEFAULT NULL COMMENT '用户关联id',
  `status` smallint(2) DEFAULT '0' COMMENT '0、未提交1、已提交2经理审核、3总监审核',
  `aggregate` decimal(5,2) DEFAULT NULL COMMENT '等级评分 0<D<=60,60<C<=90,90<B<=100,100<A 绩效评分：D=0.0,C=1.0,B=1.5,A=2.0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='月报表';

-- ----------------------------
-- Records of monthly
-- ----------------------------

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event` varchar(255) DEFAULT NULL,
  `status` smallint(1) DEFAULT '0' COMMENT '优先级：0、最低，1、一般，2、优先，3、加急,4、已完成',
  `type` smallint(1) DEFAULT NULL COMMENT '类型：0、自定义，1.项目任务，2、客户沟通，3、订单沟通',
  `uid` int(11) DEFAULT NULL,
  `startdate` int(10) DEFAULT NULL,
  `enddate` int(10) DEFAULT NULL,
  `content` text,
  `allday` smallint(1) DEFAULT NULL COMMENT '是否为全天待办',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='待办、任务表';

-- ----------------------------
-- Records of notes
-- ----------------------------

-- ----------------------------
-- Table structure for reports
-- ----------------------------
DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `content` text COMMENT '角色数据',
  `o` int(2) unsigned DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='报表类型表';

-- ----------------------------
-- Records of reports
-- ----------------------------

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `k` varchar(100) NOT NULL COMMENT '变量',
  `v` text NOT NULL COMMENT '值',
  `type` tinyint(1) NOT NULL COMMENT '0系统，1自定义',
  `name` varchar(255) NOT NULL COMMENT '说明',
  PRIMARY KEY (`k`),
  KEY `k` (`k`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='网站设置';

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES ('sitename', '有穹开源办公管理系统', '0', '');
INSERT INTO `setting` VALUES ('title', '工作管理系统', '0', '');
INSERT INTO `setting` VALUES ('keywords', '有穹开源办公管理系统', '0', '');
INSERT INTO `setting` VALUES ('description', '有穹开源办公管理系统', '0', '');
INSERT INTO `setting` VALUES ('footer', 'By 2016 &copy; 有穹网', '0', '');
INSERT INTO `setting` VALUES ('statistics', '', '1', '统计代码');

-- ----------------------------
-- Table structure for update
-- ----------------------------
DROP TABLE IF EXISTS `update`;
CREATE TABLE `update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='更新日志';

-- ----------------------------
-- Records of update
-- ----------------------------
INSERT INTO `update` VALUES ('1', '开发菜单功能', '初始系统情况下开发菜单管理功能', '1478066818', '1');
INSERT INTO `update` VALUES ('2', '系统功能开发', '用户权限下的用户组管理功能', '1478066818', '1');
INSERT INTO `update` VALUES ('3', '开发更新日志', '开发系统版本更新日志功能！', '1478067216', '1');
INSERT INTO `update` VALUES ('4', '系统结构整理', '完成用户组，更新日志，网站设置，菜单管理等模块结构布置！', '1478067338', '1');
INSERT INTO `update` VALUES ('5', '功能优化', '菜单管理布局调整！', '1478072594', '1');
INSERT INTO `update` VALUES ('6', '功能开发', '完成部门管理->成员管理', '1478167237', '1');
INSERT INTO `update` VALUES ('7', '新增分享功能', '分享功能：笔记可以分享，加密分享，文件可以分享加密分享', '1478674883', '1');
INSERT INTO `update` VALUES ('8', '新增到说说功能', '实现说说发布功能', '1479087732', '1');
INSERT INTO `update` VALUES ('9', '完成报表类型功能', '完成报表类型功能', '1479698466', '1');
INSERT INTO `update` VALUES ('10', '开发完成考勤管理板块', '考勤管理板块内容以及完成', '1480406761', '1');
INSERT INTO `update` VALUES ('11', '开始开发即时通讯功能', '右侧隐藏的通讯功能', '1480406795', '1');

-- ----------------------------
-- Table structure for words
-- ----------------------------
DROP TABLE IF EXISTS `words`;
CREATE TABLE `words` (
  `wid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `time` int(10) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `source` smallint(1) DEFAULT NULL COMMENT '类型：1、工作笔记，2、代码笔记，3、摘抄笔记，4、私人笔记，',
  PRIMARY KEY (`wid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='笔记表';

-- ----------------------------
-- Records of words
-- ----------------------------
INSERT INTO `words` VALUES ('1', '利用CSS实现酷炫的下拉框特效', '![](Uploads/thumb/201611/_63974.gif)\r\n\r\n想要制作这么一个效果还是比较麻烦的，但是代码并不难理解。\r\n首先，来看看 Html 代码。\r\n    \r\n    \r\n        <div class=\"container\">  \r\n            <div class=\"heading\">  \r\n                <h2>Custom Select</h2>  \r\n            </div>  \r\n          \r\n            <div class=\"select\">  \r\n                <p>Please select</p>  \r\n                <ul>  \r\n                    <li data-value=\"HTML5\">HTML5</li>  \r\n                    <li data-value=\"CSS3\">CSS3</li>  \r\n                    <li data-value=\"JavaScript\">JavaScript</li>  \r\n                    <li data-value=\"JQuery\">JQuery</li>  \r\n                    <li data-value=\"Backbone\">Backbone</li>  \r\n                </ul>  \r\n            </div>  \r\n        </div>  \r\n    \r\n    \r\n\r\n可见，我们并没有利用原生的 select 元素，而是利用其它元素来模拟这个效果。我们为 li 元素指定了 data-value,主要是接下来我们会用 JQuery 获取选中值并将其放置到 p 元素下。\r\n\r\n下面逐步来看 CSS 代码。\r\n\r\n```css\r\n\r\n\r\n    * {   \r\n        margin: 0;   \r\n        padding: 0;   \r\n    }   \r\n      \r\n    html {   \r\n        font-family: \'Terminal\';   \r\n        font-size: 62.5%;   \r\n    }   \r\n      \r\n    body {   \r\n        background-color: #33CC66;   \r\n    }  \r\n\r\n\r\n```\r\n\r\n1、将网页中所有元素的外边距和内边距设置为 0。\r\n\r\n2、将网页中的默认字体设置为 Terminal，字体大小设置为 62.5%, 也就是 10px。\r\n\r\n3、设置背景颜色为 #33CC66。\r\n\r\n```html\r\n\r\n\r\n    <link href=\'http://fonts.googleapis.com/css?family=Lobster|Terminal+Dosis\' rel=\'stylesheet\' type=\'text/css\'>  \r\n\r\n\r\n```\r\n\r\n上面我们用到了 Terminal 字体，而且接下来我们还会使用 Lobster 字体，所以用这行代码添加引用。\r\n\r\n1、指定 headng, select 宽度并指定其水平居中。\r\n\r\n2、修改 heading 的宽度，主要是为了让其宽度大于 select 的宽度，显得更加美观。然后指定其上外边距和下外边距。\r\n\r\n3、设置 heading 下 h2 元素的字体和字体大小，颜色。\r\n\r\n```html\r\n\r\n\r\n    .select > p, .select ul {   \r\n            background-color: #ffffff;   \r\n            font-size: 2rem;   \r\n            border: 1px solid bisque;   \r\n            border-radius: 5px;   \r\n            margin-bottom: 0;   \r\n        }   \r\n      \r\n    .select > p {   \r\n            text-align: left;   \r\n            padding: 1rem;   \r\n            position: relative;   \r\n            border-bottom-rightright-radius: 0;   \r\n            border-bottom-left-radius: 0;   \r\n            cursor: pointer;   \r\n            color: rgba(102, 102, 102, .6);   \r\n        }   \r\n    .select > p:after {   \r\n            display: block;   \r\n            width: 10px;   \r\n            height: 10px;   \r\n            content: \'\';   \r\n            position: absolute;   \r\n            top: 1.4rem;   \r\n            rightright: 2rem;   \r\n            border-bottom: 1px solid #33CC66;   \r\n            border-left: 1px solid #33CC66;   \r\n            transform: rotate(-45deg);   \r\n            transition: transform .3s ease-out, top .2s ease-out;   \r\n        }  \r\n\r\n\r\n```\r\n\r\n1、设置 p 和 ul 元素的背景颜色和边框等设置。\r\n\r\n2、为 p 元素单独指定样式，并设置其 position 属性，主要是为了下面绘制右侧的下拉按钮。\r\n\r\n3、利用 :after 在p 元素的右方绘制下拉按钮，可以看出来，我们是利用左下边框然后旋转 -45 度 模拟的这个效果。值得注意的是，我们需要将其  display 设置为 block，并且设置宽高，否则看不到 这个效果。\r\n\r\n```html\r\n\r\n\r\n    .select ul {   \r\n        margin-top: 0;   \r\n        border-top-left-radius: 0;   \r\n        border-top-rightright-radius: 0;   \r\n        list-style-type: none;   \r\n        cursor: pointer;   \r\n        overflow-y: auto;   \r\n        max-height: 0;   \r\n        transition: max-height .3s ease-out;   \r\n    }   \r\n      \r\n    .select ul li {   \r\n        padding-left: 0.5rem;   \r\n        display: block;   \r\n        line-height: 3rem;   \r\n        text-align: left;   \r\n    }  \r\n\r\n\r\n```\r\n\r\n1、设置 ul 的一些默认属性，并将其设置最大宽度为 0，指定 overflow-y 为 auto ，这个时候ul 将会被隐藏。\r\n\r\n2、在这里设置的时候我遇到了一个问题，就是 li 标签始终占不满 ul 的一行，那是因为其默认有 margin 和 padding ，所以在一开始的时候就将网页中所有元素的外边距和内边距设置成了 0。\r\n\r\n    \r\n    \r\n        .select.open ul {   \r\n            max-height: 20rem;   \r\n            transform-origin: 50% 0;   \r\n            -webkit-animation: slide-down .5s ease-in;   \r\n        }   \r\n          \r\n        .select.open > p:after {   \r\n            position: absolute;   \r\n            top: 1.6rem;   \r\n            transform: rotate(-225deg);   \r\n            transition: transform .3s ease-in, top .2s ease-in;   \r\n        }  \r\n    \r\n    \r\n\r\n\r\n1、为 open 设置最大高度，并为其指定动画效果。\r\n\r\n2、将下拉按钮旋转 -225 度，并为其指定动画。\r\n\r\n下面我们看看为 ul 元素指定的 slide-down 动画效果，这也是这个下拉特效的关键所在。\r\n\r\n```html\r\n@-webkit-keyframes slide-down {   \r\n    0% {   \r\n        transform: scale(1, 0);   \r\n    }   \r\n    25% {   \r\n        transform: scale(1, 1.25);   \r\n    }   \r\n    50% {   \r\n        transform: scale(1, 0.75);   \r\n    }   \r\n    75% {   \r\n        transform: scale(1, 1.1);   \r\n    }   \r\n    100% {   \r\n        transform: scale(1, 1);   \r\n    }   \r\n} \r\n```\r\n\r\n看到以上代码可能就都明白了，就是不断改变 ul 的大小，让其在 0.75-1.25 之间进行缩放，所以就会有那个跳动的效果了。\r\n\r\n下面还有一些简单的 CSS 代码，不再解释。\r\n\r\n    \r\n    \r\n        .select ul li:hover {   \r\n            background-color: rgba(102, 153, 102, 0.4);   \r\n        }   \r\n          \r\n        .select .selected {   \r\n            background-color: rgba(102, 153, 102, 0.8);   \r\n        }  \r\n    \r\n    \r\n	\r\n\r\nCSS 讲完了，下面就可以看看我们是如何利用 JQuery 控制这个效果的。\r\n\r\n我们并不需要下载 JQuery 就可以使用，因为现在已经有很多网站提供了  CDN 服务，比如我使用的 BootCDN。\r\n\r\n    \r\n    \r\n        <script src=\"http://cdn.bootcss.com/jquery/3.1.1/jquery.min.js\"></script>  \r\n    \r\n\r\n    <script>  \r\n        $(document).ready(function () {   \r\n            $(\'.select ul li\').on(\"click\", function (e) {   \r\n                var _this = $(this);   \r\n                $(\'.select >p\').text(_this.attr(\'data-value\'));   \r\n                $(_this).addClass(\'selected\').siblings().removeClass(\'selected\');   \r\n                $(\'.select\').toggleClass(\'open\');   \r\n                cancelBubble(e);   \r\n            });   \r\n      \r\n            $(\'.select>p\').on(\"click\", function (e) {   \r\n                $(\'.select\').toggleClass(\'open\');   \r\n                cancelBubble(e);   \r\n            });   \r\n      \r\n            $(document).on(\'click\', function () {   \r\n                $(\'.select\').removeClass(\'open\');   \r\n            })   \r\n        })   \r\n      \r\n        function cancelBubble(event) {   \r\n            if (event.stopPropagation) {   \r\n                event.preventDefault();   \r\n                event.stopPropagation();   \r\n            } else {   \r\n                event.returnValue = false;   \r\n                event.cancelBubble();   \r\n            }   \r\n        }   \r\n    </script>  \r\n\r\n\r\n    \r\n	\r\n	\r\n\r\n1、首先为 p 标签绑定 click 事件，在触发的时候，为 select 添加或移除 open class, 也就是将 ul 显示出来。\r\n\r\n2、为 li 绑定 click 事件，当选中了一个 li 元素的时候，首先获取到 data-value，然后将其赋值给 p 标签，然后为选中的 li 添加 selected class,与此同时利用 siblings() 方法，让兄弟节点的 selected class 移除。\r\n\r\n3、为 document 设置click 事件，当我们点击网页中任何一处地方的时候，如果 ul 是打开的，就将其关闭，不过这个时候由于所有元素都在 document 内，所以我们需要阻止事件冒泡，调用自己写的 cancelBubble() 方法。\r\n\r\n总结\r\n好了，本文的内容到这就基本介绍了，希望能对大家的学习或者工作带来一定的帮助，如果有疑问大家可以留言交流。\r\n', '1478326773', '1', '2');
INSERT INTO `words` VALUES ('2', '自行车都智能化了，你可知道它的历史？ ', ' 在不少旁观者的眼里，智能化几乎成了一种避之唯恐不及的“瘟疫”，开始攀附上大大小小、各式各样的工具和设备，从水杯、灯泡、体重秤这样的小物件，再到冰箱、洗衣机这些生活中的庞然大物。\r\n\r\n兜兜转转之后，这场“瘟疫”又找上了在生活中不那么起眼的自行车。然而，在搭上智能化的顺风车之前，你可知道自行车的历史？虽然，在乐视超级自行车的发布会上，它已经用了自行车史上有着重要地位的三个名字——斯塔利、西夫拉克、阿尔普迪埃——来命名自家的自行车，但那远远不够。Gizmodo 为我们梳理了自行车发展的关键节点。\r\n\r\n在开始之前，我们先来看看丹麦的设计师制作的动画，一分钟看完自行车近 200 年的演变。\r\n\r\n比较公认的说法是，第一辆自行车由法国手工艺人西夫拉克（Médé de Sivrac）设计，在两个轮子上安装了支架并配上马鞍，前进的话需要用脚蹬地提供动力。这还只是一个很简单的雏形，没有方向舵，转弯的时候需要骑行者下车抬起前轮，稳定性也不好。\r\n\r\n德国人杜莱斯（Karl Drais von Sauerbronn）制作了一辆和西夫拉克的设计相近的两轮车子，增加了车把，可以改变行进中的方向，速度可以达到 15km/h。不过，还是需要靠双脚蹬地提供动力。\r\n\r\n这时候第一辆真正意义上的自行车诞生了，是由苏格兰铁匠麦克米伦（Kirkpatrik Macmillan）设计的，它还有一个专门的名字——Velocipede。\r\n\r\n在不少旁观者的眼里，智能化几乎成了一种避之唯恐不及的“瘟疫”，开始攀附上大大小小、各式各样的工具和设备，从水杯、灯泡、体重秤这样的小物件，再到冰箱、洗衣机这些生活中的庞然大物。\r\n\r\n兜兜转转之后，这场“瘟疫”又找上了在生活中不那么起眼的自行车。然而，在搭上智能化的顺风车之前，你可知道自行车的历史？虽然，在乐视超级自行车的发布会上，它已经用了自行车史上有着重要地位的三个名字——斯塔利、西夫拉克、阿尔普迪埃——来命名自家的自行车，但那远远不够。Gizmodo 为我们梳理了自行车发展的关键节点。\r\n\r\n在开始之前，我们先来看看丹麦的设计师制作的动画，一分钟看完自行车近 200 年的演变。\r\n\r\n比较公认的说法是，第一辆自行车由法国手工艺人西夫拉克（Médé de Sivrac）设计，在两个轮子上安装了支架并配上马鞍，前进的话需要用脚蹬地提供动力。这还只是一个很简单的雏形，没有方向舵，转弯的时候需要骑行者下车抬起前轮，稳定性也不好。\r\n\r\n德国人杜莱斯（Karl Drais von Sauerbronn）制作了一辆和西夫拉克的设计相近的两轮车子，增加了车把，可以改变行进中的方向，速度可以达到 15km/h。不过，还是需要靠双脚蹬地提供动力。\r\n\r\n这时候第一辆真正意义上的自行车诞生了，是由苏格兰铁匠麦克米伦（Kirkpatrik Macmillan）设计的，它还有一个专门的名字——Velocipede。\r\n\r\n在不少旁观者的眼里，智能化几乎成了一种避之唯恐不及的“瘟疫”，开始攀附上大大小小、各式各样的工具和设备，从水杯、灯泡、体重秤这样的小物件，再到冰箱、洗衣机这些生活中的庞然大物。\r\n\r\n兜兜转转之后，这场“瘟疫”又找上了在生活中不那么起眼的自行车。然而，在搭上智能化的顺风车之前，你可知道自行车的历史？虽然，在乐视超级自行车的发布会上，它已经用了自行车史上有着重要地位的三个名字——斯塔利、西夫拉克、阿尔普迪埃——来命名自家的自行车，但那远远不够。Gizmodo 为我们梳理了自行车发展的关键节点。\r\n\r\n在开始之前，我们先来看看丹麦的设计师制作的动画，一分钟看完自行车近 200 年的演变。\r\n\r\n比较公认的说法是，第一辆自行车由法国手工艺人西夫拉克（Médé de Sivrac）设计，在两个轮子上安装了支架并配上马鞍，前进的话需要用脚蹬地提供动力。这还只是一个很简单的雏形，没有方向舵，转弯的时候需要骑行者下车抬起前轮，稳定性也不好。\r\n\r\n德国人杜莱斯（Karl Drais von Sauerbronn）制作了一辆和西夫拉克的设计相近的两轮车子，增加了车把，可以改变行进中的方向，速度可以达到 15km/h。不过，还是需要靠双脚蹬地提供动力。\r\n\r\n这时候第一辆真正意义上的自行车诞生了，是由苏格兰铁匠麦克米伦（Kirkpatrik Macmillan）设计的，它还有一个专门的名字——Velocipede。\r\n\r\n在不少旁观者的眼里，智能化几乎成了一种避之唯恐不及的“瘟疫”，开始攀附上大大小小、各式各样的工具和设备，从水杯、灯泡、体重秤这样的小物件，再到冰箱、洗衣机这些生活中的庞然大物。\r\n\r\n兜兜转转之后，这场“瘟疫”又找上了在生活中不那么起眼的自行车。然而，在搭上智能化的顺风车之前，你可知道自行车的历史？虽然，在乐视超级自行车的发布会上，它已经用了自行车史上有着重要地位的三个名字——斯塔利、西夫拉克、阿尔普迪埃——来命名自家的自行车，但那远远不够。Gizmodo 为我们梳理了自行车发展的关键节点。\r\n\r\n在开始之前，我们先来看看丹麦的设计师制作的动画，一分钟看完自行车近 200 年的演变。\r\n\r\n比较公认的说法是，第一辆自行车由法国手工艺人西夫拉克（Médé de Sivrac）设计，在两个轮子上安装了支架并配上马鞍，前进的话需要用脚蹬地提供动力。这还只是一个很简单的雏形，没有方向舵，转弯的时候需要骑行者下车抬起前轮，稳定性也不好。\r\n\r\n德国人杜莱斯（Karl Drais von Sauerbronn）制作了一辆和西夫拉克的设计相近的两轮车子，增加了车把，可以改变行进中的方向，速度可以达到 15km/h。不过，还是需要靠双脚蹬地提供动力。\r\n\r\n这时候第一辆真正意义上的自行车诞生了，是由苏格兰铁匠麦克米伦（Kirkpatrik Macmillan）设计的，它还有一个专门的名字——Velocipede。 ', '1478323877', '1', '3');
INSERT INTO `words` VALUES ('3', '探索Vue.js component内容实现', '现在来系统地学习一下Vue（参考vue.js官方文档）:\r\n\r\nVue.js是一个构建数据驱动的web界面的库，其目标是实现响应的数据绑定和组合的试图组件。\r\n\r\nVue.js拥抱数据驱动的视图概念，这意味着我们能在普通的HTML模板中使用特殊的用法将DOM“绑定”到底层数据。一旦创建了绑定，DOM将于数据保持同步。\r\n\r\n以下参考代码与上面的模型相对应\r\n    <!-- 这是我们的 View -->\r\n    <div id=\"example-1\">\r\n       Hello {{ name }}!\r\n    </div>\r\n    // 这是我们的 Model\r\n    var exampleData = {\r\n       name: \'Vue.js\'\r\n    }\r\n      \r\n    // 创建一个 Vue 实例或 \"ViewModel\"\r\n    // 它连接 View 与 Model\r\n    var exampleVM = new Vue({\r\n       el: \'#example-1\',  // 在一个id为\'example-1\'的实体上挂载\r\n       data: exampleData  // 数据流\r\n    })\r\n	\r\n\r\n通常我们会把Model写在Vue实例当中，下面写法与上面写法效果一样：\r\n    <!-- 这是我们的 View -->\r\n    <div id=\"example-1\">\r\n       Hello {{ name }}!  <!--- Vue的数据模型用{{datamodel}}包裹起来 --->\r\n    </div>\r\n      \r\n    // 创建一个 Vue 实例或 \"ViewModel\"\r\n    // 它连接 View 与 Model<br><script>\r\n    var exampleVM = new Vue({\r\n       el: \'#example-1\',  // 在一个id为\'example-1\'的实体上挂载\r\n       data: {\r\n           name: \'Vue.js\'\r\n          } // 数据流\r\n    })<br></script>\r\n	\r\n	\r\n\r\n这样一段程序执行后，在#example-1这个控件中就会显示‘Hello Vue.js!\'。\r\n\r\n下面来看看指令(Directives)：\r\n指令是特殊的带有前缀 v- 的特性，指令的值限定为绑定表达式，比如一个if的指令：\r\n\r\n`<p v-if=\"greeting\">hello!<p>`\r\n\r\n还有绑定指令，即将某些属性值与一些值相绑定，比如：\r\n\r\n`<input :type = \"questItem.type\", :name = \"questItem.name\"/>`\r\n\r\n这里省略了“v-bind”，使得input的属性值赋值具有“计算”的效果。　\r\n\r\n计算属性\r\n\r\n这里介绍一下$watch的用法，用于当一个数据需要根据其他的数据而变化时的情况：\r\n\r\n    <script><br>var vm = new Vue({\r\n     el: \'#demo\',\r\n     data: {\r\n      firstName: \'Foo\',\r\n      lastName: \'Bar\',\r\n      fullName: \'Foo Bar\'\r\n     }\r\n    })<br></script>\r\n      \r\n    vm.$watch(\'firstName\', function (val) { // 当firstname改变时，立马更新vm.fullname\r\n     this.fullName = val + \' \' + this.lastName\r\n    })\r\n      \r\n    vm.$watch(\'lastName\', function (val) {  // 当lastname改变时，立马更新vm.fullname\r\n     this.fullName = this.firstName + \' \' + val\r\n    })\r\n\r\n\r\n在这里，所有的数据的对象都可以通过vm.firstname等来访问。　　\r\n\r\nv-model　　\r\n\r\n顾名思义，就是Vue当中的数据模型，它用来绑定Vue实例中的数据：\r\n\r\n    <!--- bi-direction bound --->\r\n    <div id=\"app\">\r\n      <p>{{message}}</p>\r\n      <input v-model=\"message\"> <!--Model，input输入的数据会立即反馈到Vue实例中--> \r\n    </div>\r\n    <script>\r\n      new Vue({\r\n        el: \'#app\',  // View\r\n        data: {\r\n          message: \'Hello Vue.js\'\r\n        }\r\n      })\r\n    </script>\r\n	\r\n\r\n比如要用来绑定一个表单控件，就是把选择的值显示出来：\r\n\r\n    <!---    表单控件绑定-单选 --->\r\n    <div id=\"myselect\">  // 外面这一层我一开始没有加，然后就出错了，el好像一般是挂载在<div>构件上\r\n    <select v-model=\"selected\"> // data的数据类型是selected，值是选取的值\r\n      <option seleceted>A</option>\r\n      <option>B</option>\r\n      <option>C</option>\r\n    </select>\r\n    <span>Selected: {{ selected }}</span>\r\n    </div>\r\n      \r\n    <script>\r\n      new Vue({\r\n        el: \'#myselect\',\r\n        data:{\r\n          selected:[]\r\n        }\r\n      })\r\n    </script>', '1478324107', '1', '2');
INSERT INTO `words` VALUES ('4', '原生javascript实现的ajax异步封装功能示例', '```javascript\r\n<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\r\n\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>无标题文档</title>\r\n<script type=\"text/javascript\" src=\"Scripts/jquery.js\"></script>\r\n</head>\r\n<style>\r\n* { margin: 0px; padding: 0px; }\r\n#box { float: left; width: 500px; }\r\n#left { float: left; background: #090; width: 100px; height: 100px; }\r\n#right { background: #C60; width: 100px; height: 100px; float: left; }\r\n#box2 { width: 180px; height: 100px; }\r\nhtml>body #box2 { width: auto; height: auto; min-width: 180px; min-height: 100px; }\r\n</style>\r\n<body>\r\n<div id=\"box\">\r\n <div id=\"left\">点击我 看效果！</div>\r\n <div id=\"right\">fffeeee</div>\r\n</div>\r\n<div style=\"width:100px; height:100px; background:#969; float:left;\" id=\"dd\">dddd</div>\r\n<script>\r\n// 异步请求封装 IE6即以上浏览器\r\n// ajax(url,fnSucc,selectID,fnFaild)\r\n//url 请求地址\r\n//fnSucc 异步请求后的内容处理函数\r\n//fnFaild 请求失败处理函数\r\nfunction ajax(url,fnSucc,fnFaild)\r\n{\r\n    //1.创建Ajax对象\r\n    //非IE6\r\n    var oAjax;\r\n    if(window.XMLHttpRequest)//不会报错，只会是undefined\r\n     {oAjax=new XMLHttpRequest();}\r\n    else\r\n    //iE6 IE5\r\n     {oAjax=new ActiveXObject(\"Microsoft.XMLHTTP\");}\r\n    //alert(oAjax);\r\n    //2.连接服务器\r\n    //open(方法,文件名,异步传输)\r\n    oAjax.open(\"get\",url,true);//制止缓存\r\n    //3.发送请求\r\n    oAjax.send();\r\n    //4.接收返回值 和服务器通讯的时候此事件发生\r\n    oAjax.onreadystatechange=function()\r\n    {\r\n     //oAjax.readyState //浏览器和服务器，进行到哪一步了 异步握手过程\r\n     if(oAjax.readyState==4)//读取完成（可能文件不存在）\r\n     {\r\n      if(oAjax.status==200 || oAjax.status==304)//请求成功 304即使浏览器缓存了也返回数据\r\n      {\r\n       fnSucc(oAjax.responseText);\r\n       //alert(\"成功\"+oAjax.responseText);\r\n      }\r\n      else\r\n      {\r\n       if(fnFaild)//fnFaild传进来时\r\n       {\r\n        fnFaild(oAjax.status);\r\n       }\r\n       //alert(\"失败:\"+oAjax.status);//status为404\r\n      }\r\n     }\r\n    }\r\n}\r\nwindow.onload=function(){\r\n  var oBtn=document.getElementById(\"left\");\r\n  oBtn.onclick=function()\r\n  {\r\n      ajax(\"http://28967904.jsp.jspee.cn/ext/singlePage/list/json-1-1-20\",function(str){\r\n        var da= JSON.parse(str); //JSON数据解析\r\n        alert(da.totalRow)\r\n        },function(erorr){\r\n          console.log(\'请求出错：\'+erorr);\r\n        })\r\n  }\r\n}\r\n</script>\r\n</body>\r\n</html>\r\n```\r\n\r\n注释：实践才是老师！', '1478329331', '1', '4');
INSERT INTO `words` VALUES ('5', '笔记使用说明书实例教程', '# 笔记使用说明\r\n\r\n**目录 (Table of Contents)**\r\n\r\n[TOCM]\r\n\r\n[TOC]\r\n\r\n# Heading 1\r\n## Heading 2               \r\n### Heading 3\r\n#### Heading 4\r\n##### Heading 5\r\n###### Heading 6\r\n# Heading 1 link [Heading link](Public/markdown/editor.md \"Heading link\")\r\n## Heading 2 link [Heading link](Public/markdown/editor.md \"Heading link\")\r\n### Heading 3 link [Heading link](Public/markdown/editor.md \"Heading link\")\r\n#### Heading 4 link [Heading link](Public/markdown/editor.md \"Heading link\") Heading link [Heading link](Public/markdown/editor.md \"Heading link\")\r\n##### Heading 5 link [Heading link](Public/markdown/editor.md \"Heading link\")\r\n###### Heading 6 link [Heading link](Public/markdown/editor.md \"Heading link\")\r\n\r\n#### 标题（用底线的形式）Heading (underline)\r\n\r\nThis is an H1\r\n=============\r\n\r\nThis is an H2\r\n-------------\r\n\r\n### 字符效果和横线等\r\n                \r\n----\r\n\r\n~~删除线~~ <s>删除线（开启识别HTML标签时）</s>\r\n*斜体字*      _斜体字_\r\n**粗体**  __粗体__\r\n***粗斜体*** ___粗斜体___\r\n\r\n上标：X<sub>2</sub>，下标：O<sup>2</sup>\r\n\r\n**缩写(同HTML的abbr标签)**\r\n\r\n> 即更长的单词或短语的缩写形式，前提是开启识别HTML标签时，已默认开启\r\n\r\nThe <abbr title=\"Hyper Text Markup Language\">HTML</abbr> specification is maintained by the <abbr title=\"World Wide Web Consortium\">W3C</abbr>.\r\n\r\n### 引用 Blockquotes\r\n\r\n> 引用文本 Blockquotes\r\n\r\n引用的行内混合 Blockquotes\r\n                    \r\n> 引用：如果想要插入空白换行`即<br />标签`，在插入处先键入两个以上的空格然后回车即可，[普通链接](http://localhost/)。\r\n\r\n### 锚点与链接 Links\r\n\r\n[普通链接](http://localhost/)\r\n\r\n[普通链接带标题](http://localhost/ \"普通链接带标题\")\r\n\r\n直接链接：<https://github.com>\r\n\r\n[锚点链接][anchor-id] \r\n\r\n[anchor-id]: http://www.baidu.com/\r\n\r\n[mailto:test.test@gmail.com](mailto:test.test@gmail.com)\r\n\r\nGFM a-tail link @pandao  邮箱地址自动链接 test.test@gmail.com  www@vip.qq.com\r\n\r\n> @pandao\r\n\r\n### 多语言代码高亮 Codes\r\n\r\n#### 行内代码 Inline code\r\n\r\n执行命令：`npm install marked`\r\n\r\n#### 缩进风格\r\n\r\n即缩进四个空格，也做为实现类似 `<pre>` 预格式化文本 ( Preformatted Text ) 的功能。\r\n\r\n    <?php\r\n        echo \"Hello world!\";\r\n    ?>\r\n    \r\n预格式化文本：\r\n\r\n    | First Header  | Second Header |\r\n    | ------------- | ------------- |\r\n    | Content Cell  | Content Cell  |\r\n    | Content Cell  | Content Cell  |\r\n\r\n#### JS代码　\r\n\r\n```javascript\r\nfunction test() {\r\n	console.log(\"Hello world!\");\r\n}\r\n \r\n(function(){\r\n    var box = function() {\r\n        return box.fn.init();\r\n    };\r\n\r\n    box.prototype = box.fn = {\r\n        init : function(){\r\n            console.log(\'box.init()\');\r\n\r\n			return this;\r\n        },\r\n\r\n		add : function(str) {\r\n			alert(\"add\", str);\r\n\r\n			return this;\r\n		},\r\n\r\n		remove : function(str) {\r\n			alert(\"remove\", str);\r\n\r\n			return this;\r\n		}\r\n    };\r\n    \r\n    box.fn.init.prototype = box.fn;\r\n    \r\n    window.box =box;\r\n})();\r\n\r\nvar testBox = box();\r\ntestBox.add(\"jQuery\").remove(\"jQuery\");\r\n```\r\n\r\n#### HTML 代码 HTML codes\r\n\r\n```html\r\n<!DOCTYPE html>\r\n<html>\r\n    <head>\r\n        <mate charest=\"utf-8\" />\r\n        <meta name=\"keywords\" content=\"Editor.md, Markdown, Editor\" />\r\n        <title>Hello world!</title>\r\n        <style type=\"text/css\">\r\n            body{font-size:14px;color:#444;font-family: \"Microsoft Yahei\", Tahoma, \"Hiragino Sans GB\", Arial;background:#fff;}\r\n            ul{list-style: none;}\r\n            img{border:none;vertical-align: middle;}\r\n        </style>\r\n    </head>\r\n    <body>\r\n        <h1 class=\"text-xxl\">Hello world!</h1>\r\n        <p class=\"text-green\">Plain text</p>\r\n    </body>\r\n</html>\r\n```\r\n\r\n### 图片 Images\r\n\r\nImage:\r\n\r\n![](Public/markdown/examples/images/4.jpg)\r\n\r\n> Follow your heart.\r\n\r\n![](Public/markdown/examples/images/8.jpg)\r\n\r\n> 图为：厦门白城沙滩\r\n\r\n图片加链接 (Image + Link)：\r\n\r\n[![](Public/markdown/examples/images/7.jpg)](Public/markdown/images/7.jpg \"李健首张专辑《似水流年》封面\")\r\n\r\n> 图为：李健首张专辑《似水流年》封面\r\n                \r\n----\r\n\r\n### 列表 Lists\r\n\r\n#### 无序列表（减号）Unordered Lists (-)\r\n                \r\n- 列表一\r\n- 列表二\r\n- 列表三\r\n     \r\n#### 无序列表（星号）Unordered Lists (*)\r\n\r\n* 列表一\r\n* 列表二\r\n* 列表三\r\n\r\n#### 无序列表（加号和嵌套）Unordered Lists (+)\r\n                \r\n+ 列表一\r\n+ 列表二\r\n    + 列表二-1\r\n    + 列表二-2\r\n    + 列表二-3\r\n+ 列表三\r\n    * 列表一\r\n    * 列表二\r\n    * 列表三\r\n\r\n#### 有序列表 Ordered Lists (-)\r\n                \r\n1. 第一行\r\n2. 第二行\r\n3. 第三行\r\n\r\n#### GFM task list\r\n\r\n- [x] GFM task list 1\r\n- [x] GFM task list 2\r\n- [ ] GFM task list 3\r\n    - [ ] GFM task list 3-1\r\n    - [ ] GFM task list 3-2\r\n    - [ ] GFM task list 3-3\r\n- [ ] GFM task list 4\r\n    - [ ] GFM task list 4-1\r\n    - [ ] GFM task list 4-2\r\n                \r\n----\r\n                    \r\n### 绘制表格 Tables\r\n\r\n| 项目        | 价格   |  数量  |\r\n| --------   | -----:  | :----:  |\r\n| 计算机      | $1600   |   5     |\r\n| 手机        |   $12   |   12   |\r\n| 管线        |    $1    |  234  |\r\n                    \r\nFirst Header  | Second Header\r\n------------- | -------------\r\nContent Cell  | Content Cell\r\nContent Cell  | Content Cell \r\n\r\n| First Header  | Second Header |\r\n| ------------- | ------------- |\r\n| Content Cell  | Content Cell  |\r\n| Content Cell  | Content Cell  |\r\n\r\n| Function name | Description                    |\r\n| ------------- | ------------------------------ |\r\n| `help()`      | Display the help window.       |\r\n| `destroy()`   | **Destroy your computer!**     |\r\n\r\n| Left-Aligned  | Center Aligned  | Right Aligned |\r\n| :------------ |:---------------:| -----:|\r\n| col 3 is      | some wordy text | $1600 |\r\n| col 2 is      | centered        |   $12 |\r\n| zebra stripes | are neat        |    $1 |\r\n\r\n| Item      | Value |\r\n| --------- | -----:|\r\n| Computer  | $1600 |\r\n| Phone     |   $12 |\r\n| Pipe      |    $1 |\r\n                \r\n----\r\n\r\n#### 特殊符号 HTML Entities Codes\r\n\r\n&copy; &  &uml; &trade; &iexcl; &pound;\r\n&amp; &lt; &gt; &yen; &euro; &reg; &plusmn; &para; &sect; &brvbar; &macr; &laquo; &middot; \r\n\r\nX&sup2; Y&sup3; &frac34; &frac14;  &times;  &divide;   &raquo;\r\n\r\n18&ordm;C  &quot;  &apos;\r\n\r\n[========]\r\n\r\n### Emoji表情 :smiley:\r\n\r\n> Blockquotes :star:\r\n\r\n#### GFM task lists & Emoji & fontAwesome icon emoji & editormd logo emoji :editormd-logo-5x:\r\n\r\n- [x] :smiley: @mentions, :smiley: #refs, [links](), **formatting**, and <del>tags</del> supported :editormd-logo:;\r\n- [x] list syntax required (any unordered or ordered list supported) :editormd-logo-3x:;\r\n- [x] [ ] :smiley: this is a complete item :smiley:;\r\n- [ ] []this is an incomplete item [test link](#) :fa-star: @pandao; \r\n- [ ] [ ]this is an incomplete item :fa-star: :fa-gear:;\r\n    - [ ] :smiley: this is an incomplete item [test link](#) :fa-star: :fa-gear:;\r\n    - [ ] :smiley: this is  :fa-star: :fa-gear: an incomplete item [test link](#);\r\n \r\n#### 反斜杠 Escape\r\n\r\n\\*literal asterisks\\*\r\n\r\n[========]\r\n            \r\n### 科学公式 TeX(KaTeX)\r\n\r\n$$E=mc^2$$\r\n\r\n行内的公式$$E=mc^2$$行内的公式，行内的$$E=mc^2$$公式。\r\n\r\n$$x > y$$\r\n\r\n$$\\(\\sqrt{3x-1}+(1+x)^2\\)$$\r\n                    \r\n$$\\sin(\\alpha)^{\\theta}=\\sum_{i=0}^{n}(x^i + \\cos(f))$$\r\n\r\n多行公式：\r\n\r\n```math\r\n\\displaystyle\r\n\\left( \\sum\\_{k=1}^n a\\_k b\\_k \\right)^2\r\n\\leq\r\n\\left( \\sum\\_{k=1}^n a\\_k^2 \\right)\r\n\\left( \\sum\\_{k=1}^n b\\_k^2 \\right)\r\n```\r\n\r\n```katex\r\n\\displaystyle \r\n    \\frac{1}{\r\n        \\Bigl(\\sqrt{\\phi \\sqrt{5}}-\\phi\\Bigr) e^{\r\n        \\frac25 \\pi}} = 1+\\frac{e^{-2\\pi}} {1+\\frac{e^{-4\\pi}} {\r\n        1+\\frac{e^{-6\\pi}}\r\n        {1+\\frac{e^{-8\\pi}}\r\n         {1+\\cdots} }\r\n        } \r\n    }\r\n```\r\n\r\n```latex\r\nf(x) = \\int_{-\\infty}^\\infty\r\n    \\hat f(\\xi)\\,e^{2 \\pi i \\xi x}\r\n    \\,d\\xi\r\n```\r\n\r\n### 分页符 Page break\r\n\r\n> Print Test: Ctrl + P\r\n\r\n[========]\r\n\r\n### 绘制流程图 Flowchart\r\n\r\n```flow\r\nst=>start: 用户登陆\r\nop=>operation: 登陆操作\r\ncond=>condition: 登陆成功 Yes or No?\r\ne=>end: 进入后台\r\n\r\nst->op->cond\r\ncond(yes)->e\r\ncond(no)->op\r\n```\r\n\r\n[========]\r\n                    \r\n### 绘制序列图 Sequence Diagram\r\n                    \r\n```seq\r\nAndrew->China: Says Hello \r\nNote right of China: China thinks\\nabout it \r\nChina-->Andrew: How are you? \r\nAndrew->>China: I am good thanks!\r\n```\r\n\r\n### End', '1478329319', '1', '4');
INSERT INTO `words` VALUES ('6', '尝试作文800字：那是一次成功的尝试', '天才毕竟难得，没有谁能不用学习就万事皆通。再伟大的人也得从说话、走路学起，再浩大的工程也得一步一个脚印，凡事都有第一次。第一次总是让人跃跃欲试，总是让人激动万分，总是让人忐忑不安……\r\n\r\n成长总是艰难的，幸好只是“万事开头难”，还记得那些让你难以忘怀的第一次吗？还记得那些让你兴奋不已的成功的尝试吗？\r\n\r\n1、《那是一次成功的尝试》\r\n\r\n在一个人的一生中，有成功也有失败。也许失败占百分之九十九，而成功只占百分之一，所以说成功是难能可贵的，成功的滋味也是不同寻常的。我也成功过，也曾尝到过成功的滋味。\r\n\r\n小时候有一次我去学游泳，以为游泳很容易，哪里知道学起来并不轻松。爷爷首先跃进了游泳池，我站在上面不敢下水。爷爷鼓励我说：不要怕，你下水来试试，不下水是学不会游泳的。我在爷爷的鼓励下，下到了水里，可水刚刚没过胸口，便感到了水的压力。它把我肚子挤得瘪瘪的，喘气时要用很大的气力，鼻子不知干什么去了，吸气吃力，只有张大嘴巴大口呼吸，似乎要吞食什么东西，很难受。\r\n\r\n后来，我在水里学走路，我想学游泳也没什么大不了的，只要大口吸气，身体就会轻轻松松地漂起来，再刨几下，用脚划划水，就会“扬帆前航”了。可自己的腿仿佛注了铅一般，抬起来非常吃力。首先要在水里学会憋气，整个脑袋都在水里，而且一不小心就会吃到一口水，我吗，大概可以憋二十秒，我想，我还要继续努力，向三十秒前进。\r\n\r\n然后就开始学游泳的基本功了，我的手像船浆一样从中间向四周划开，脚一上一下来回舞动着，还要憋气，刚扑腾了两三下，自我感觉还不错，有点骄傲了，这时我感到了身体在慢慢下沉。于是，我的心一慌，手脚全乱了，接着就又喝了一口水，那滋味儿真不好受后来，爷爷帮助我，把我拉上岸来，对我说：“你的毛病就是换气、划水和蹬腿不协调的结果，只要坚持下去就能成功。”\r\n\r\n我不再气馁，坚信“铁杵也能磨成针”，我决心再次下水，一定要把游泳学会。我深深地吸了一口气，猛地一下钻入水中，接着就按照爷爷教我的要领一点点地划了起来。果然，爷爷教我的要领很有效，我坚持游了下来。在那一刻，我尝到成功的滋味。\r\n\r\n后来，爷爷奶奶看见我会游泳了，都夸我厉害，我自己也开始沾沾自喜了，这时爷爷要趁热打铁，赶紧教我仰泳，让我再会一种方法。当然，仰泳我也学会了。\r\n\r\n在回家的路上我一路都很开心，因为我尝到了成功的滋味了。那是一次成功的尝试！\r\n\r\n2、《那是一次成功的尝试》\r\n\r\n童年如梦，是欢乐的，是美好的。童年的岁月中，我经历过多少风风雨雨，每一次成功，都有尝试，不尝试怎么能有收获呢？记得，那是一次成功的尝试。\r\n\r\n我是一个十来岁的孩子，到现在，我已经会做一点大人们的事了，炒菜，买东西，照顾弟弟妹妹……我都做得没有一丝马虎，做得妥妥当当。这样的功劳，少不了那次的尝试。\r\n\r\n说出来你可别笑话，我从小就胆小如鼠，做什么事都得大人陪着，直到九岁，我才开始尝试自己去买东西，自己做饭，还有自己睡觉呢。\r\n\r\n有一次，正巧碰上爸爸妈妈上班，保姆也回家了，家里只剩下九岁的我和四岁的妹妹。我们正忧愁着晚饭的事，不能做太难的菜，也不能不合我们的口味。打开冰箱，只见里面整整齐齐的都是肉菜什么的，可没有一样是我会做的。我突然想到了蒸水蛋，忙往鸡蛋盒里望，可望来望去没望出个结果，仔细一瞧，大事不好——每鸡蛋了。这可怎么办呢？我绞尽脑汁，只想出唯一一条不好使的方法——买。可有担心有坏人，有担忧钱不够。做好准备，我惟有“告别”了妹妹，尝试着自己出去买鸡蛋了啦。唉，真没办法。\r\n\r\n握着手里的那十块钱，我迈着胆怯怯的脚步，尝试着第一次出去买东西的滋味。一路上，天都是黑沉沉的，没有一丝灯光，只在月亮的明亮的光下，我走着走着，穿出了小巷，终于看到了灯光，可路上的行人用着凶恶的眼神望着我，似乎我做了什么坏事一样，我的心就像十五个水桶打水——七上八下，腿也有点不好使了，每走一步，总得震一震脚。\r\n\r\n平时短短的路程，今天似乎变的漫长极了。我正为自己担心着：“第一次尝试出来买东西，会不会遇到坏人？会不会遇到一个不讲理的老板？会不会突然窜出一只狗狗，和我”拼死拼活“，对我死追不放呢？”又想：第一次出来买东西，当然要勇敢一点啦。好不容易，才来到鸡蛋铺门前，我的心像是有只兔子在蹦蹦跳，“咚咚”的打起鼓来。我鼓足勇气，才结结巴巴地说：“我……我想……买一斤鸡蛋。”老板毫不犹豫地给了我一斤鸡蛋，我拿着鸡蛋，付了钱，不顾一切地跑了回家。这才如释重负。\r\n\r\n这一次尝试买东西，我成功了，我尝试到当家的滋味。在成长的经历中，我一定记住这一次尝试，在人生的道路上，迈上光辉的一步！\r\n\r\n3、《那是一次成功的尝试》\r\n\r\n我经历过一次成功的尝试。那次尝试是我人生的新起点，那次，我超越了自己，那次，我终生难忘！\r\n\r\n那次尝试就发生在去年寒假，我参加英语集训营，每天都会有一节发音大课，也就是在第三次上发音大课时，我成功地……\r\n\r\n和往常一样，李老师在台上十分投入地教我们如何发音，我在台下专心致志地听讲。这是，李老师却突然要求每班派一名同学上台背课文，我照样不举手，虽然我背得滚瓜烂熟，因为我会紧张，一紧张就失语，就会引来一大片人的嘲笑，尽管我是曾在台上风光一时的训练有素的舞蹈演员，尽管我曾有一段属于自己的舞台灯光，可是我从来就不敢独自上台，而且是成为台上焦点。“D1班怎么没有人上台？”我这才注意到我们班没人举手，“哪个愿意自告奋勇，就高举小手！”我犹豫了，手慢慢往上升，可马上就缩了回来，我的内心在挣扎，到底是该还是不该呢？“没有人了吗，那就要视为弃权了，我数五声，五、四……”在李老师的一再催促下，我急了，为了我们班，我豁出去了，就在他数“一”的那一刹那间，我胆怯地举起右手，李老师马上点我上台，还没有完全做好准备的我迈着沉重的步子缓慢地走上去。唉，这回得丢脸喽，真是糟糕，早知道我就不举手了，我现在什么也想不起来了！我怎么这么鲁莽。可开弓没有回头箭，我只好硬着头皮走上台。\r\n\r\nA1的大哥哥，A2的大姐姐很流利地背了出来，我一看，心里更没底了，更加后悔自己举手，此刻牙齿打颤，双腿发抖，心怦怦直跳，在寒冷的气候里，我竟然在流汗，身子是热的，心却是冷的。轮到我了，我往台下一看，发现同班同学和老师向我投来鼓励的目光，为我打气：“一定要为我们班争光啊！”其他人的目光齐刷刷地看过来，紧张气氛的萦绕，使单词一个个堵在喉咙里，怎么也吐不出来，我深吸一口气，努力使自己平静下来：不就是背课文嘛，就当台下没人。紧接着竟通顺如流水般地背出来，连我自己都不敢相信自己的耳朵。李老师宣布成绩，我们班获得了第一名，全场爆发出热烈的掌声，此时我兴奋得快要窒息，第一次上台，我成功了。原来成功并不是梦啊，只要大胆去试，就不会失败！\r\n\r\n通过这次上台的经历，我觉得只要你肯，你敢，不论或是失败，都会有所收获，只要你愿意尝试，超越自己，那么，你就是大赢家！\r\n\r\n4、《那是一次成功的尝试》\r\n\r\n上个星期六，我和叔叔阿姨与表妹一起去爬圌山，在路上，我和表妹打赌：输的人就是小狗，说着，我们俩拉了拉钩，我心中暗暗窃喜：“嘿嘿，这次我赢定了，谁叫我比你大六岁呢。”\r\n\r\n可到了山脚下我就后悔了，那圌山山势险峻，我看后不禁打了一个寒颤，我真想收回那该死的赌，可谁叫我逞能呢？万一爬到山中间突然滑下来……唉，不想了，面对现实吧，再看表妹，他她似乎毫不畏惧，我心想：“切，我算什么姐姐啊，胆子比妹妹都小，不行，我必须勇敢点，否则，我就糗大了。”\r\n\r\n刚开始爬，我的心“扑通扑通”跳，都快跳到嗓子眼了，可我毕竟比表妹大，如果让她看到我现在这幅胆小如鼠的样子，肯定会笑话我的。“姐姐，你发什么楞啊？我超过你了哦！”我这时才反应过来，表妹已经超过我了，于是，我什么都不顾，管她淑女不淑女的，心中只有一个念头：超过表妹。{呵呵，其实我还是有点害怕的啦，不过假如我输了就是小狗，我才不要那么丢脸呢。}\r\n\r\n爬到中间时，有一个滑坡，只有经过了滑坡才能继续往前爬，可滑坡下竟都是石头……望着底下，我全身发抖，鸡皮疙瘩都竖起来了，心里就如十五只吊桶打水-----七上八下，我想了想后果，不仅望而却步，这时，一个稚嫩的声音在我耳边响起：“姐姐，你不会害怕了吧？”我猛地一惊，为了不让自己丢脸，我连忙摆手说：“才不是呢，我不会当缩头乌龟的。”说完，我只好硬着头皮上了。我抓住旁边的树木，心紧揪，慢慢走了下来。眼看我与成功仅有一步之遥，不能就此放弃了，过了一会儿，我发现自己已经过了滑坡，耶！我成功了！我全身立即放松了下来，感觉心里正开着无数灿烂的小花。\r\n\r\n渐渐的，我不害怕了，终于到了山顶，因为我比表妹大啦，所以胜者当然是我喽。我俯视下面，花儿正竞相开放，树木苍翠，我呼吸着新鲜的空气，吹着凉风，啊，舒服极了，我真想大喊一声：我成功了！\r\n\r\n5、《那是一次成功的尝试》\r\n\r\n人生中有无数次尝试，每一次尝试都是珍贵的回忆。在这条记忆的长河中，令我印象最深的就是那次和外国人交流的事。回想起来，那天的画面又浮现在我的脑海中。\r\n\r\n那天，舅舅的杂货店里来了一位老外。金黄的卷发，蓝色的眼睛，一下子吸引了周围不少人。老外微笑着说：“Hello!I want to buy a bottle of water.”这下，可难道了舅舅。舅舅犹如丈二的和尚摸不着头脑。于是，就来请教我这个“小老师”。\r\n\r\n我一听，震住了。自己平时在学校从不好好学习英语，可以称得上是“哑巴英语”，和老师对话都答不上来，更何况是和一个老外呢！我急忙推辞舅舅的建议：“不行，不行，我的英语太烂了，那个外国友人一定听不懂我的话。”我坚定的回答到。\r\n\r\n这时，在一旁的舅妈沉不住气了，说到：“孩子，那个外国人还在外面等着，我看你还是去试试吧！做任何事都需要尝试，你连试都没有试过，怎么知道自己不行呢？再说，你平时英语口语不好，不就是因为不相信自己，不开口和别人对话而导致的吗？”我犹豫不决，一旁的舅舅又插嘴说：“是啊，是啊！你看周围的人都来了，我们总不能把那个外国人给赶走吧！”我一想：的确如此，趁这个机会还能好好地练习一下口语呢！想到这，我一口答应了舅舅的请求。\r\n\r\n我低着头，胆战心惊地走到外国友人的面前，壮着胆子说：“Hello，can I help you?”外国人一听，紧皱的眉头终于舒展开了，惊讶的问到：“Can you speak English?”\r\n\r\n我点点头。他又说：“I want to buy a bottle of water.”我愣住了，什么？“瓦特”？“瓦特”是什么东西？我一时什么也想不起来，手心里全是汗，我感觉自己的心好像要跳出来了，脸涨得通红。外国友人似乎看出了我没有听懂他的话，又重复了一遍：“I want to buy a bottle of water.”结果依旧是如此，我还是听不懂他说得话。我心急如焚，想着补救的方法。我原以为外国人要离开，可没想到，他拿出了一张纸和一支笔，画了一瓶水。\r\n\r\n我这才恍然大悟，原来外国友人要的是水。我急忙拿来了一瓶矿泉水递给他。他又询问到：“How much is it?”“2yuan.”我自信满满的回答道。买到了水，外国友人的脸上终于再次出现了笑脸：“Bye.”“Bye.”\r\n\r\n送走了老外后，我这才松了一口气。而这次小小的尝试也让我深深地懂得：尝试是任何一件事成功的法宝，只有尝试过才不会后悔。\r\n\r\n6、《成功的尝试》\r\n\r\n打开我记忆的相册，许多往事就像那一张张泛黄的照片，随着岁月的流逝，渐渐地模糊不清。但那件事却像一张彩照，清晰地印在我的记忆中，那就是我第一次尝试骑自行车的情景。\r\n\r\n那是一个阳光明媚的午后，我缠着表姐到公园教我骑自行车。刚到公园，我就迫不及待的骑了上去。谁知车子一歪。差点摔倒了。表姐一把拉住我说：“别急，慢慢来。你要一脚踩在脚蹬子上，另一脚踩在地上滑几步，然后再快速地坐上去。来试试，我扶着你。”我按照表姐的话去做，果然成功的上了车。在表姐的帮助和指挥下，我骑得越来越稳了，可我渐渐感觉后面支撑的力量不见了，我回头一看，表姐不知道什么时候松了手，我一紧张，车子又开始剧烈的摇晃起来，我重重地摔在了地上。“你怎么松手了？害我腿都摔疼了！”我大声责怪表姐，眼泪也不争气地流了下来。可表姐却若无其事地说：“你已经会练了，接下来就该你自己练了。”说完就坐在一旁休息了。“不管就不管，我自己练！”我赌气的说着。\r\n\r\n我按照刚才练习的方法尝试着独立骑自行车，开始的时候摇晃得厉害，但我脑中不断想着表姐的话：“紧握车把，掌握好方向，目视前方。”渐渐地，车子不晃了，我骑得越来越稳。黄昏的时候，我终于学会了骑自行车，我高兴地向表姐炫耀：“没有你的帮助，我自己也会骑了。”表姐只是笑了笑，什么也没有说。\r\n\r\n那天晚上，我在表姐的日记中看到这样一句话：“今天，我教表妹骑自行车，看到她摔倒时我很心疼，但我知道，如果我一直扶着她，她是学不会的。有些事，只能她自己去尝试，希望她不会生我的气。”看到这儿，我的内心充满了感动，原来是我误会表姐了。\r\n\r\n至今我还一直记着表姐的那句话：有些事只能自己去尝试。是的，那次成功的体验极大的鼓舞了我，当我面对困难和挫折时，这句话就会鼓励我勇敢地尝试，克服困难，向成功努力！\r\n\r\n7、《这是一次成功的尝试》\r\n\r\n光阴似箭，白驹过隙。不知不觉中，我已过了第一个本命年。在这12年里，我不知有过多少次尝试：尝试阅读外文书籍，尝试制作电影，尝试写作……不过，最值得一提的还是这一次。\r\n\r\n这学期的第一天，我们踏着灿烂的朝霞，走进宽敞明亮的教室。“铃——”数学老师脸上带着微笑，踏着铃声走进教室，高兴地对我们说：“同学们，新学期开始了。我觉得你们应该更加丰富自己的课余生活。所以，我决定给你们每人发一副数棋。你们先自己钻研，10天后在班里开始比赛，采取循环赛制，列积分榜，争夺冠亚军。”\r\n\r\n“好——”大家异口同声地响应。\r\n\r\n此后，我们一有时间就钻研数棋。我发现，数棋并不难。\r\n\r\n10天后，我们班拉开了“班级数棋大赛”的帷幕。此后，一下课，你就可以看见，我们班教室里许多桌子上都摆着一盘数棋，2人对下，还有许多人观战。墙上的积分榜上，每个人的分数都清清楚楚，老师作为裁判，每天都要核分。\r\n\r\n不久后，比赛结束了。积分榜前3名分别是：我、安蕾和郑毅。我们仨聚在一起，庆贺了一番后，做出了一个“惊天”的决定：组建“梦之队”挑战数学老师。我们和老师联系后，决定于教师节对阵。作文\r\n\r\n一连许久，我们仨天天聚在一起探讨下数棋的方法，并互相切磋，寻找自己的弱点和别人的优点，互相取长补短。我研究了他俩的走法，再揣摩《数棋指南》，选取其中精彩的部分与自己的走法相结合，琢磨出自己的棋路。\r\n\r\n教师节那天，我们“梦之队”相互鼓励着手拉手默记着挑战“要诀”走进了教师办公室。数学老师早已笑眯眯地等着我们了，见我们进来，便让我们坐下，给我们每人倒了杯果汁。\r\n\r\n“准备好了吗？”老师笑眯眯地问我们。\r\n\r\n“准备好了。”\r\n\r\n“好，那么，开始吧。”老师摆开棋局。\r\n\r\n郑毅在我们的鼓励下坐到了老师对面，但我分明看见他的“奔儿头”上沁出一层密密的汗珠。“完了。”我和安蕾对视了一眼。\r\n\r\n果然不出所料，郑毅过于紧张，竟落下了一个棋子。最终，手忙脚乱的他以一百多分之差完败。\r\n\r\n因为“先锋”郑毅败得太惨，所以第二个出阵的安蕾也十分紧张，她细长的手指颤抖着拿起棋子，连落子都落不准了，结果败得更惨。\r\n\r\n轮到我了，我虽有些紧张，但心里默念着“镇静！镇静！”，开始与老师对弈。刚开局，我们你来我往“斗”了许多回合，仍未能分出高低。我愈发紧张，嘴唇都咬破了。\r\n\r\n这时，我恰好一眼瞧见了老师的一个破绽，抓住机会连走几步好棋，便逐渐占据了主动。\r\n\r\n又过了许久，随着我最后一个棋子入位轻碰棋盘的声音，这局棋结束了。我和老师的分数相等，但老师还有一子未入位，惜败。\r\n\r\n“很好。这是我收到的最好的教师节礼物。希望你们不仅在棋艺上，在其他方面也能青出于蓝而胜于蓝。”老师欣慰地与我们碰杯，一口喝干了满满一杯果汁。\r\n\r\n“好的。”我们使劲地点了点头。\r\n\r\n“天啊，伊蓓，你是怎么做到的？”刚出教师办公室，安蕾便迫不及待地问我。\r\n\r\n“其实，我只不过把这次挑战当成了向老师学习数棋的机会，当成了一次尝试，胜败并不很看重。再加上临场超常发挥，我就侥幸胜了。”我掩饰不住内心的喜悦。\r\n\r\n“原来如此……”安蕾又疑惑地问，“那，你为什么下得这么好？”\r\n\r\n“以前我们‘备战’时，我已琢磨出了许多走法。又在你们与老师对阵时，仔细研究了老师的棋路，针对老师的走法‘对症下药’，剑走偏锋。所以……”我回答。\r\n\r\n“Good！”郑毅竖起大拇指。\r\n\r\n这时，我才发现，安蕾穿着她最心爱的公主裙。\r\n\r\n……\r\n\r\n我们兴奋地走在操场上，继续探讨着数棋的走法。天边泛起几抹晚霞，大大的操场上只剩下了小小的我们。斜阳把我们的影子拉得很长很长……', '1478364146', '1', '3');

-- ----------------------------
-- Table structure for works
-- ----------------------------
DROP TABLE IF EXISTS `works`;
CREATE TABLE `works` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `start` int(10) DEFAULT NULL COMMENT '开始时间',
  `end` int(10) DEFAULT NULL COMMENT '结束时间',
  `rid` int(11) DEFAULT NULL COMMENT '类型id',
  `uid` int(11) DEFAULT NULL,
  `status` smallint(1) unsigned DEFAULT '0' COMMENT '0未审核,1已审核,2不通过',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='工作记录表';

-- ----------------------------
-- Records of works
-- ----------------------------
