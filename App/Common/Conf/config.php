<?php
return array(
	//'配置项'=>'配置值'
    'MODULE_ALLOW_LIST'  => array('Home','Team'),
    'DEFAULT_MODULE'     => 'Home', //默认模块
    'MULTI_MODULE'       => true,//隐藏模块true显示模块 false 隐藏模块
    'URL_MODEL'          => '2', //URL模式
    'SESSION_AUTO_START' => true, //是否开启session
    'URL' =>'', //网站根URL
    'LOAD_EXT_CONFIG'   => 'db,path,debug,route', //加载更多配置

);