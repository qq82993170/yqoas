<?php
return array(
    //数据库链接配置
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => '127.0.0.1', // 服务器地址
    'DB_NAME'   => 'business', // 数据库名
    'DB_USER'  => '用户名',// 数据库用户名
    'DB_PWD'  => '密码',// 数据库密码
    'DB_PORT'   => 3306, // 端口
    'DB_PREFIX' => '', // 数据库表前缀
    'DB_CHARSET'=>  'utf8',      // 数据库编码默认采用utf8
);
