<?php
namespace Home\Controller;
use Common\Controller\BaseController;
use Think\Auth;

class ComController extends BaseController
{
    //公共函数
    public function _initialize()
    {
        C(setting());

    }

    //公共的验证码
    public function verify()
    {
        $config =    array(
            'fontSize' => 14, // 验证码字体大小
            'length' => 4, // 验证码位数
            'useNoise' => false, // 关闭验证码杂点
            'fontttf' => '1.ttf' ,
            'imageW'=>100,
            'imageH'=>30,
            'bg'       =>  array(243,251,254),
        );
        $Verify =     new \Think\Verify($config);
        $Verify->entry();
    }

}