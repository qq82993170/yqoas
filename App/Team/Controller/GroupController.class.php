<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：用户组控制器
 */

namespace Team\Controller;
use Think\Controller;
class GroupController extends ComController
{
    public function index(){
        $group = M('auth_group')->select();
        $this->assign('list',$group);
        $this->assign('nav',array('user','grouplist','grouplist'));//导航
        $this -> display();
    }


    public function stu(){

        //获取id
        $gid = I('post.gid');
        //实例化模型
        $m = M('auth_group');
        //查找他的状态码
        $res = $m->where("id=$gid")->field('status')->find();
        //修改信息
        if($res['status']=='0'){
            if($m->where("id=$gid")->save(array('status'=>1))){
                $info = array(
                    "info"=>"恭喜，用户组已启用！",
                    "tips"=>"恭喜，您已经永久启用了这个用户组！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }

        }else{
            if($m->where("id=$gid")->save(array('status'=>0))){
                $info = array(
                    "info"=>"恭喜，用户组已禁用！",
                    "tips"=>"恭喜，您已经永久禁用了这个用户组！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }
        }

    }

    public function del(){

        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:false;
        $ids = json_decode($ids,true);
        if(!empty($ids)){
            if(is_array($ids)){
                foreach($ids as $k=>$v){
                    $ids[$k] = intval($v['id']);
                }
                $ids = implode(',',$ids);
                $map['id']  = array('in',$ids);
                if(M('auth_group')->where($map)->delete()){
                    addlog('删除用户组ID：'.$ids);
                    $info = array(
                        "info"=>"恭喜操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }else{
                    $info = array(
                        "info"=>"参数错误",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }else{
                $info = array(
                    "info"=>"参数错误",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }
        }else{
            $info = array(
                "info"=>"您还没有选择要删除的用户组！",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }


    }

    public function update(){

        $data['title'] = isset($_POST['title'])?trim($_POST['title']):false;
        $id = isset($_POST['id'])?intval($_POST['id']):false;
        if($data['title']){
            $status = isset($_POST['status'])?$_POST['status']:'';
            if(empty($status)){
                $data['status'] = 0;
            }else{
                $data['status'] = 1;
            }
            $rules = isset($_POST['rules'])?$_POST['rules']:0;
            if(is_array($rules)){
                foreach($rules as $k=>$v){
                    $rules[$k] = intval($v);
                }
                $rules = implode(',',$rules);
            }
            $data['rules'] = $rules;
            if($id){
                $group = M('auth_group')->where('id='.$id)->data($data)->save();
                if($group){
                    addlog('编辑用户组，ID：'.$id.'，组名：'.$data['title']);
                    $info = array(
                        "info"=>"恭喜，用户组修改成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                    exit(0);
                }else{
                    $info = array(
                        "info"=>"未修改内容！",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }else{
                M('auth_group')->data($data)->add();
                addlog('新增用户组，ID：'.$id.'，组名：'.$data['title']);
                $info = array(
                    "info"=>"恭喜，用户组添加成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
                exit(0);
            }
        }else{
            $info = array(
                "info"=>"用户组名称不能为空！",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }
    }

    public function edit(){

        $id = isset($_GET['id'])?intval($_GET['id']):false;
        if(!$id){
            $this->ajaxReturn('参数错误！');
        }

        $group = M('auth_group')->where('id='.$id)->find();
        if(!$group){
            $this->ajaxReturn('参数错误！');
        }
        //获取所有启用的规则
        $rule = M('auth_rule')->field('id,pid,title')->where('status=1')->order('o asc')->select();
        $group['rules'] = explode(',',$group['rules']);
        $rule = $this->getMenu($rule);
        $this->assign('rule',$rule);
        $this->assign('group',$group);
        $this->assign('nav',array('user','grouplist','addgroup'));//导航
        $this -> display();
    }

    public function add(){

        //获取所有启用的规则
        $rule = M('auth_rule')->field('id,pid,title')->where("status=1")->order('o asc')->select();
        $rule = $this->getMenu($rule);
        $this->assign('rule',$rule);
        $this -> display();
    }

}