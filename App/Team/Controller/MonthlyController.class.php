<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：月报管理控制器
 */

namespace Team\Controller;
use Think\Controller;
class MonthlyController extends ComController
{
    //月报列表
    public function index($p=1)
    {

        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $where = 'monthly.uid='.$uid;

        //搜索条件
        if($keyword <> ''){
            $where .=" AND monthly.my_assess LIKE '%$keyword%' OR monthly.my_duty LIKE '%$keyword%' OR monthly.my_active LIKE '%$keyword%' OR monthly.my_observe LIKE '%$keyword%'";
        }

        $count = M('monthly')->where($where)->count();
        $list = M('monthly')->field('monthly.*,member.name')
            ->join('member on member.uid=monthly.uid','left')
            ->where($where)
            ->order('monthly.date desc')
            ->limit($offset.','.$pagesize)
            ->select();

        $page	=   new \Think\Page($count,$pagesize);
        $page = $page->show();

        $this->assign('list',$list);
        $this->assign('page',$page);

        $this->display();

    }

    //月报提交
    public function add()
    {
        $month = strtotime(date('Y-m',time()));
        $uid = $this->USER['uid'];
        $data = M('monthly')->where('date='.$month.' AND uid='.$uid)->find();
        $this->assign('data',$data);

        $this->display();

    }

    public function edit($id){

        $id = intval($id);
        $uid = $this->USER['uid'];
        $data = M('monthly')->where('id='.$id.' AND uid='.$uid)->find();
        $this->assign('data',$data);

        $this->display('add');
    }

    public function audit($id){

        $id = intval($id);
        $data = M('monthly')->where('id='.$id)->find();
        $this->assign('data',$data);

        $this->display();
    }

    public function update(){

        $month = date('Y-m',time());
        $data = I('post.','');
        $data['date'] = strtotime($month);
        $data['uid'] = isset($_POST['uid'])?intval($_POST['uid']):$this->USER['uid'];

        if($data['id']){
            if(M('monthly')->data($data)->where("id='{$data['id']}'")->save()){
                addlog('修改'.$month.'月报报表，ID：'.$data['id']);
                $info = array(
                    "info"=>"恭喜,操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $info = array(
                    "info"=>"内容无修改,无需保存现有内容！",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

        }else{

            $id =  M('monthly')->data($data)->add();
            if($id){
                addlog('填写'.$month.'月报报表，ID：'.$id);
                $info = array(
                    "info"=>"恭喜,操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $info = array(
                    "info"=>"参数错误！",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

        }

    }
    
}