<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：部门管理控制器
 */

namespace Team\Controller;
use Think\Controller;
use Vendor\Tree;
class DepartmentController extends ComController
{
    //部门列表
    public function index(){

        $department = M('department')->field('id,pid,name,time')->order('time desc')->select();
        $department = $this->getMenu($department);
        $this->assign('department',$department);
        $this -> display();
    }

    public function del(){

        $id = isset($_POST['id'])?intval($_POST['id']):false;
        if($id){
            $data['id'] = $id;
            $department = M('department');
            $member = M('member');
            if($department->where('pid='.$id)->count() || $member->where('dept_id='.$id)->count()){
                $info = array(
                    "info"=>"删除失败，有子部门或是有用户存在当前部门下",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $department->where('id='.$id)->delete();
                addlog('删除部门，ID：'.$id);
            }
            $info = array(
                "info"=>"恭喜，操作成功！",
                "status"=>"y",
            );
            $this -> ajaxReturn($info,'json');
        }else{
            $info = array(
                "info"=>"删除失败，可能是程序未获取到参数",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    //添加界面
    public function add(){

        $pid = isset($_GET['pid'])?intval($_GET['pid']):0;
        $department = M('department')->field('id,pid,name,time')->order('time asc')->select();
        $tree = new Tree($department);
        $str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
        $department = $tree->get_tree(0,$str, $pid);

        $this->assign('department',$department);
        $this -> display();
    }



    //编辑界面
    public function edit($id=0)
    {
        $id = intval($id);
        $m = M('department');
        $data = $m->where("id='$id'")->find();
        $department = M('department')->field('id,pid,name')->order('time asc')->select();
        $tree = new Tree($department);
        $str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
        $department = $tree->get_tree(0,$str, $data['pid']);

        $this->assign('department',$department);
        $this->assign('data',$data);
        $this->display('edit');
    }

    //数据添加
    public function update(){
        $id = I('post.id','','intval');
        $data['name'] = I('post.name','','strip_tags');
        $data['pid'] =  isset($_POST['pid'])?trim($_POST['pid']):'';
        $data['content'] =  isset($_POST['content'])?trim($_POST['content']):'';
        if($id){
            if($data['pid']==$id){
                $info = array(
                    "info"=>"父级部门不可以是当前部门",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                M('department')->data($data)->where("id='{$id}'")->save();
                addlog('编辑部门，ID：'.$id);
            }
        }else{
            $data['time'] =  time();
            M('department')->data($data)->add();
            addlog('新增部门，名称：'.$data['name']);
        }
        $info = array(
            "info"=>"恭喜,操作成功！",
            "status"=>"y",
        );
        $this -> ajaxReturn($info,'json');
    }

}