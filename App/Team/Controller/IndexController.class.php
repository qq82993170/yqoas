<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：员工空间与首页界面控制器
 */
namespace Team\Controller;
use Think\Controller;
class IndexController extends ComController
{
    //OA系统管理首頁
    public function index()
    {
        //操作信息数据
        $user = $this->USER['user'];
        $log = M('log')->where("name='{$user}'")->order('t desc')->limit(11)->select();
        $this->assign('log',$log);

        //获取待办数据
        $uid=$this->USER['uid'];
        $toDay= strtotime(date('Y-m-d', time()));
        $tmr  = strtotime(date('Y-m-d', strtotime("+1 day")));
        $notes = M('notes')->where("uid='{$uid}' and startdate >={$toDay} and startdate <{$tmr} and status !=4")->order('startdate asc')->limit(6)->select();
        $noteCount = M('notes')->where("uid='{$uid}' and startdate >={$toDay} and startdate <{$tmr} and status !=4")->count();
        $this->assign('notes',$notes);
        $this->assign('noteCount',$noteCount);

        //获取最新通告数据
        $inform = M('inform')->where('end >'.$toDay)->order('end desc')->limit('30')->select();
        $this->assign('inform',$inform);

        //获取最新共享数据
        $share = M('dynamic')
            ->field('member.name as uname,dynamic.*')
            ->join('member on member.uid=dynamic.uid','left')
            ->where("dynamic.source = '2'")
            ->order('dynamic.id desc')
            ->limit('30')->select();
        $this->assign('share',$share);

        $this->display();
    }

    //退出登录
    public function logout()
    {

        cookie('auth',null);
        $url = U("/");
        header("Location: {$url}");
        exit(0);

    }


    //日志信息
    public function logs($p=1){

        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $options = isset($_GET['options'])?$_GET['options']:'';

        $p = intval($p)>0?$p:1;
        $user = $this->USER['user'];
        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量

        //获取日期条件
        switch($options){
            case 'day':
                $today = strtotime(date('Y-m-d',time()));
                $where = "name='{$user}' AND t >='{$today}'";
                break;

            case 'week':
                $week = strtotime('last week');
                $where = "name='{$user}' AND t >='{$week}'";
                break;

            case 'month':
                $month = strtotime("-1 month");
                $where = "name='{$user}' AND t >='{$month}'";
                break;

            default:
                $where = "name='{$user}'";
                break;
        }
        //搜索条件
        if($keyword <> ''){
            $where .=" AND log LIKE '%$keyword%' OR ip LIKE '%$keyword%'";
        }

        $count = M('log')->where($where)->count();
        $list = M('log')->where($where)->order('t desc')->limit($offset.','.$pagesize)->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();

        $this->assign('list',$list);
        $this->assign('page',$page);

        $this->display();

    }

    //通告查看
    public function inform($id=''){

        $id = intval($id);

        $inform = M('inform')->where("id='{$id}'")->order("id desc")->find();

        //上一篇
        $prev =  M('inform')->where("id < $id")->order('id desc')->limit('1')->find();
        $this ->assign("prev",$prev);
        //下一篇
        $next =  M('inform')->where("id > $id")->order('id asc')->limit('1')->find();
        $this ->assign("next",$next);

        $this->assign('inform',$inform);
        $this->display();

    }


}