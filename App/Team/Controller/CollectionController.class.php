<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：收藏夹管理控制器
 */
namespace Team\Controller;
use Think\Controller;
class CollectionController extends ComController
{
    public function index($p=1){

        $status = isset($_GET['status'])?intval($_GET['status']):0;
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("参数错误！");
        }
        $collection = M('collection');
        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $prefix = C('DB_PREFIX');
        $where = "{$prefix}collection.uid=$uid";
        if($status <> ''){
            $where .= " AND {$prefix}collection.status=$status";
        }
        if($keyword){
            $where .=" AND {$prefix}collection.content LIKE '%$keyword%' OR {$prefix}collection.name LIKE '%$keyword%'";
        }
        $count = $collection->where($where)->count();
        $list  = $collection->where($where)->order("time desc")->limit($offset.','.$pagesize)->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();

        $this->assign('list',$list);
        $this->assign('page',$page);
        $this -> display();
    }

    public function del(){

        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:false;
        $ids = json_decode($ids,true);
//        dump($ids);die;
        if($ids){
            if(is_array($ids)){
                foreach($ids as $k=>$v){
                    $ids[$k] = intval($v['id']);
                }
                $ids = implode(',',$ids);
                $map['id']  = array('in',$ids);
                if(M('collection')->where($map)->delete()){
                    addlog('删除收藏夹ID：'.$ids);
                    $info = array(
                        "info"=>"恭喜操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }else{
                    $info = array(
                        "info"=>"参数错误",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }else{
                $info = array(
                    "info"=>"参数错误",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }
        }else{
            $info = array(
                "info"=>"警告！参数错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    public function stu(){

        $id = isset($_REQUEST['id'])?$_REQUEST['id']:false;
//        dump($ids);die;
        if($id){

                $data =M('collection')->field("status")->where("id=".$id)->find();
                //如果标记为已经标记则清空，没有标记则标记
                if(!$data['status']){
                    $data['status'] = 1;
                    $logInfo =  '标记收藏夹ID：'.$id;
                }else{
                    $data['status'] = 0;
                    $logInfo =  '清空收藏夹ID：'.$id;
                }
                //更新数据
                if(M('collection')->where("id=".$id)->save($data)){
                    addlog($logInfo);
                    $info = array(
                        "info"=>"恭喜操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }else{
                    $info = array(
                        "info"=>"参数错误",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }

        }else{
            $info = array(
                "info"=>"警告！参数错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    public function view($id= 0 ){

        $id = intval($id);
        $uid = $this->USER['uid'];
        $collection = M('collection')
            ->field("collection.*,member.name as author")
            ->join("member on member.uid=collection.uid",'LEFT')
            ->where("collection.id <='{$id}' and collection.uid ='{$uid}'")
            ->order('id desc')
            ->limit(15)
            ->select();
        $this->assign('collection',$collection);
        $this->display();

    }

    //收藏夹列表
    public function add(){


        $this -> display();
    }

    public function edit($id = 0){

        $id = intval($id);
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("状态错误！");
        }

        $collection = M('collection')->where("id='{$id}'")->find();
        $this -> assign('collection',$collection);
        $this -> display();

    }

    public function update($id=0){

        $id = intval($id);
        $data['name'] = isset($_POST['name'])?$_POST['name']:false;
        $data['status'] = isset($_POST['status'])?$_POST['status']:0;
        $data['content'] = isset($_POST['content'])?$_POST['content']:false;
        $uid = $this->USER['uid'];

        if(!$uid){
            $info = array(
                "info"=>"警告！状态错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }
        else{
            $data['uid'] = $uid;

            if(!$data['name'] or !$data['content']){
                $info = array(
                    "info"=>"警告！收藏夹名字及收藏夹内容为必填项目。",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

            if($id){
                M('collection')->data($data)->where('id='.$id)->save();
                addlog('编辑收藏夹，id：'.$id);
                $info = array(
                    "info"=>"恭喜！收藏夹编辑成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }
            else{
                $data['time'] = time();
                $id = M('collection')->data($data)->add();
                if($id){
                    addlog('新增收藏夹，id：'.$id);
                    $info = array(
                        "info"=>"恭喜,操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }else{
                    $info = array(
                        "info"=>"抱歉，未知错误！",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }

            }

        }

    }

}