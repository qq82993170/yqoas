<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：更新日志管理控制器
 */
namespace Team\Controller;
use Think\Controller;
class UpdateController extends ComController
{
    //更新日志
    public function index()
    {
        $t = time()-3600*24*568;//获取多少天前的更新数据
        $data =M('update')->Field('member.name,update.id,update.uid,update.title,update.content,update.time')
            ->join("left join member on member.uid=update.uid")
            ->where("update.time > '{$t}'")
            ->order('update.time desc')
            ->select();
        $this->assign('time',$t);
        $this->assign('data',$data);
//        dump($data);die;
        $this->display();
    }

    //添加更新日志
    public function update(){
        if(IS_POST){
            $data = $_POST;
            $data['title'] = isset($_POST['title'])?strip_tags($_POST['title']):'';
            $data['content'] = isset($_POST['content'])?strip_tags($_POST['content']):'';
            $data['time'] = time();
            $data['uid'] = isset($_POST['uid'])?strip_tags($_POST['uid']):'';
            //添加数据
            M('update')->data($data)->add();
            addlog('添加更新日志。');
            $info = array(
                "info"=>"程序日志更新成功！",
                "status"=>"y",
            );
            $this -> ajaxReturn($info,'json');
        }
    }
}