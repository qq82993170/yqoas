<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：便签管理控制器
 */
namespace Team\Controller;
use Think\Controller;
class NotesController extends ComController
{
    //便签列表
    public function index()
    {

        $this->display();
    }

    //json数据
    public function json($start =0,$end=0){

            $uid = $this->USER['uid'];
            $data = M("notes")->where("uid={$uid} AND startdate >= '{$start}'")->select();
//        dump($data);
            $json = array();
            foreach($data as $k=>$v){
                $json[$k]['id'] = $v['id'];
                $json[$k]['title'] = $v['event'];
                $json[$k]['start'] = $v['startdate'];
                $json[$k]['end'] = $v['enddate'];
                $json[$k]['url'] ='';
                $json[$k]['allDay'] = intval($v['allday']);
                $json[$k]['color'] = getColor($v['status']);
            }

            echo json_encode($json,true);

    }


    //删除
    public function del(){

        $id = isset($_POST['id'])?$_POST['id']:false;
        if($id){
            if(M('notes')->where('id='.$id)->delete()){
                addlog('删除待办任务，ID：'.$id);
                $this->ajaxReturn(1);
            }

        }

        $this->ajaxReturn('error');

    }

    //获取弹出事件
    public function event($action='',$date=''){

        $type = $this->USER['type'];
        switch($action){
            case 'edit':
                $info = '编辑代办' ;
                $id = isset($_GET['id'])?$_GET['id']:false;
                break;

            default:
                $info = '新建代办' ;
        }
        if($type){
            $dept = $this->USER['dept_id'];
            $member = M('member')->where("type < {$type} AND dept_id={$dept}")->order("uid desc")->select();
        }
        if($id){
            $notes = M("notes")->where("id={$id}")->find();
        }
//        dump($notes);
        $this->assign('notes',$notes);
        $this->assign('member',$member);
        $this->assign('info',$info);
        $this->assign('action',$action);
        $this->assign('date',$date);
        $this->display();
    }

    //更新数据
    public function update(){

        $uid = isset($_POST['uid'])?$_POST['uid']:false;
        if(!$uid){
            $data['uid'] = $this->USER['uid'];
        }else{
            $data['uid'] = $uid;
        }
        $id = isset($_POST['id'])?$_POST['id']:false;
        $events = stripslashes(trim($_POST['event']));//事件内容
        $data['event']=strip_tags($events); //过滤HTML标签，并转义特殊字符

        $isallday = $_POST['isallday'];//是否是全天事件
        $isend = $_POST['isend'];//是否有结束时间

        $data['content'] = isset($_POST['content'])?$_POST['content']:'';
        $data['type'] = isset($_POST['type'])?$_POST['type']:0;
        $data['status'] = isset($_POST['status'])?$_POST['status']:0;

        $startdate = trim($_POST['start']);//开始日期
        $enddate = trim($_POST['end']);//结束日期

        $s_time = $_POST['s_hour'].':'.$_POST['s_minute'].':00';//开始时间
        $e_time = $_POST['e_hour'].':'.$_POST['e_minute'].':00';//结束时间

        if($isallday==1 && $isend==1){
            $data['startdate'] = strtotime($startdate);
            $data['enddate'] = strtotime($enddate);
        }elseif($isallday==1 && $isend==""){
            $data['startdate'] = strtotime($startdate);
        }elseif($isallday=="" && $isend==1){
            $data['startdate'] = strtotime($startdate.' '.$s_time);
            $data['enddate'] = strtotime($enddate.' '.$e_time);
            if($data['enddate'] < $data['startdate']){
                $this->ajaxReturn("开始时间不能大于结束时间！");
            }
        }else{
            $data['startdate'] = strtotime($startdate.' '.$s_time);
        }

        $data['allday'] = $isallday?1:0;
        //id不存在添加数据id存在更新数据
        if(!$id){

            $id = M('notes')->data($data)->add();
            if($id){
                addlog('新增待办任务，ID：'.$id);
                $this -> ajaxReturn(1);
            }else{
                $this -> ajaxReturn('写入失败！');
            }

        }
        else{

            M('notes')->data($data)->where('id='.$id)->save();
            addlog('变更待办，ID：'.$id);
            $this -> ajaxReturn(1);

        }


    }
}