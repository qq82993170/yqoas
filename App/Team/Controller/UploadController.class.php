<?php
/**
*
* 功能说明：文件上传控制器。
*
**/

namespace Team\Controller;
use Think\Controller;

class UploadController extends ComController{
    public function index($type = null){

    }
    private function saveimg($file){
        $uptypes=array(
            'image/jpeg',
            'image/jpg',
            'image/jpeg',
            'image/png',
            'image/pjpeg',
            'image/gif',
            'image/bmp',
            'image/x-png'
        );
        $max_file_size=2000000;     //上传文件大小限制, 单位BYTE
        $destination_folder='Uploads/thumb/'.date('Ym').'/'; //上传文件路径
        if($max_file_size < $file["size"]){
            echo "文件太大!";
            return null;
        }
        if(!in_array($file["type"], $uptypes)){
            echo "文件类型不符!".$file["type"];
            return null;
        }
        if(!file_exists($destination_folder)){
            mkdir($destination_folder);
        }
        $filename=$file["tmp_name"];
        $image_size = getimagesize($filename);
        $pinfo=pathinfo($file["name"]);
        $ftype=$pinfo['extension'];
        $destination = $destination_folder.time().".".$ftype;
        if (file_exists($destination)){
            echo "同名文件已经存在了";
            return null;
        }
        if(!move_uploaded_file ($filename, $destination)) {
            return null;
        }
        return "/".$destination;
    }
    public function uploadpic(){
       $Img=I('Img');
       $Path=null;
       if($_FILES['img']){
            $Img=$this->saveimg($_FILES['img']);
       }
       $BackCall=I('BackCall');
       $Width=I('Width');
       $Height=I('Height');
       if(!$BackCall)$Width=$_POST['BackCall'];
       if(!$Width)$Width=$_POST['Width'];
       if(!$Height)$Width=$_POST['Height'];
       $this->assign('Width',$Width);
       $this->assign('BackCall',$BackCall);
       $this->assign('Img',$Img);
       $this->assign('Height',$Height);
       $this->display('Uploadpic');
    }
    public function batchpic(){
       $ImgStr=I('Img');
       $ImgStr=trim($ImgStr,'|');
      $Img=array();
      if(strlen($ImgStr)>1)$Img=explode('|',$ImgStr);
       $Path=null;
       if($_FILES['img']){
          $filename=$this->saveimg($_FILES['img']);
          array_push($Img, $filename);
          $ImgStr=$ImgStr.'|'.$filename;
//          $Img=$this->saveimg($_FILES['img']);
       }
       $BackCall=I('BackCall');
       $Width=I('u');
       $Height=I('Height');
       if(!$BackCall)$Width=$_POST['BackCall'];
       if(!$Width)$Width=$_POST['Width'];
       if(!$Height)$Width=$_POST['Height'];
       $this->assign('Width',$Width);
       $this->assign('BackCall',$BackCall);
       $this->assign('ImgStr',$ImgStr);
       $this->assign('Img',$Img);
       $this->assign('Height',$Height);
       $this->display('Batchpic');
    }
	
	//文件部分
	private function savefile($file){
        $uptypes=array(
            'application/pdf',
            'application/msword',
            'text/x-msdownload',
        );
        $max_file_size=20000000;     //上传文件大小限制, 单位BYTE
        $destination_folder='Uploads/pdf/'; //上传文件路径
        if($max_file_size < $file["size"]){
            echo "文件太大!";
            return null;
        }
        if(!in_array($file["type"], $uptypes)){
            echo "文件类型不符!".$file["type"];
            return null;
        }
        if(!file_exists($destination_folder)){
            mkdir($destination_folder);
        }
        $filename=$file["tmp_name"];
        $image_size = getimagesize($filename);
        $pinfo=pathinfo($file["name"]);
        $ftype=$pinfo['extension'];
        $destination = $destination_folder.time().".".$ftype;
        if (file_exists($destination)){
            echo "同名文件已经存在了";
            return null;
        }
        if(!move_uploaded_file ($filename, $destination)) {
            return null;
        }
        return "/".$destination;
    }
	
    public function uploadfile(){
       $Files=I('Files');
       $Path=null;
       if($_FILES['file']){
            $Files=$this->savefile($_FILES['file']);
       }
       $BackCall=I('BackCall');
       $Width=I('Width');
       $Height=I('Height');
       if(!$BackCall)$Width=$_POST['BackCall'];
       if(!$Width)$Width=$_POST['Width'];
       if(!$Height)$Width=$_POST['Height'];
       $this->assign('Width',$Width);
       $this->assign('BackCall',$BackCall);
       $this->assign('Files',$Files);
       $this->assign('Height',$Height);
       $this->display('Uploadfile');
    }
	
    public function batchfile(){
       $ImgStr=I('Files');
       $ImgStr=trim($ImgStr,'|');
       $Files=array();
     if(strlen($ImgStr)>1)$Files=explode('|',$ImgStr);
       $Path=null;
       if($_FILES['file']){
          $filename=$this->saveimg($_FILES['file']);
          array_push($Files, $filename);
          $ImgStr=$ImgStr.'|'.$filename;
       }
       $BackCall=I('BackCall');
       $Width=I('u');
       $Height=I('Height');
       if(!$BackCall)$Width=$_POST['BackCall'];
       if(!$Width)$Width=$_POST['Width'];
       if(!$Height)$Width=$_POST['Height'];
       $this->assign('Width',$Width);
       $this->assign('BackCall',$BackCall);
       $this->assign('ImgStr',$ImgStr);
       $this->assign('Files',$Files);
       $this->assign('Height',$Height);
       $this->display('Batchfile');
	   
    }
	
	
}
