<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：员工空间管理控制器
 */
namespace Team\Controller;
use Think\Controller;
use Vendor\Tree;
class DzoneController extends ComController
{
    //空间首页
    public function index()
    {
        //获取用户资料
        $uid = isset($_GET['uid'])?intval($_GET['uid']):$this->USER['uid'];
        $user = M("member");
        $prefix = C('DB_PREFIX');
        if($uid){
            $people = $user->field("{$prefix}member.*,{$prefix}auth_group.id as gid,{$prefix}auth_group.title,{$prefix}department.name as dept_name")
                ->order("{$prefix}member.t desc")
                ->join("left join {$prefix}auth_group_access ON {$prefix}member.uid = {$prefix}auth_group_access.uid")
                ->join("left join {$prefix}auth_group ON {$prefix}auth_group.id = {$prefix}auth_group_access.group_id")
                ->join("left join {$prefix}department ON {$prefix}department.id = {$prefix}member.dept_id")
                ->where("member.uid={$uid}")->find();
        }else{
            $this->ajaxReturn("参数错误");
        }
        //获取分享点赞总数
        $shareCount = M('dynamic')->where("uid={$uid} and source='2'")->count();
        $prezCount = M('dynamic')->where("uid={$uid}")->sum('prez');

        $this->assign('prezCount',$prezCount);
        $this->assign('shareCount',$shareCount);
        $this->assign('people',$people);
        $this->display();
    }


    //发说说功能
    public function rlstalk(){

        if(IS_POST){
            $data['title'] =isset($_POST['title'])?$_POST['title']:'';
            $data['content'] =isset($_POST['content'])?$_POST['content']:'';
            $data['source'] = 3;//说说动态
            $data['type']=1;//文章类型
            $data['time']=time();//当前时间
            $data['uid'] =$this->USER['uid'];
            //添加数据
            if(M('dynamic')->data($data)->add()){
                $info=array(
                    'info'=>'恭喜，操作成功！',
                    'status' =>'y',
                );
                $this->ajaxReturn($info,'json');
            }
        }

        $info=array(
            'info'=>'警告参数错误！',
            'status'=>'n',
        );
        $this->ajaxReturn($info,'json');


    }


    //首页显示动态
    public function ajaxMany(){

        $uid = !empty($_POST['uid'])?intval($_POST['uid']):$this->USER['uid'];
//        dump($uid);die;
        //查询所有动态数据
        if($uid !== $this->USER['uid']){
            $where = "dynamic.uid={$uid}";
        }
        $p = isset($_POST['p'])?intval($_POST['p']):1;
        $pagesize = 8;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        //默认动态列表数据
        $list = M('dynamic')
            ->field("member.head,member.name as author,dynamic.*")
            ->join("member on member.uid=dynamic.uid","left")
            ->where($where)
            ->order('dynamic.time desc')
            ->limit($offset.','.$pagesize)
            ->select();
        if($list){
            $data = '';
            foreach($list as $k=>$v){
                $data.='<div class="'.'feed-element'.'">'
                    .'<a href="/team-dzone-index?uid='.$v['uid'].'" class="pull-left">'
                    .'<img alt="image" class="img-circle" src="'.$v['head'].'">'
                    .'</a>'
                    .'<div class="media-body ">'
                    .'<small class="pull-right text-navy">'.beenTime($v['time']).'</small>'
                    .'<strong>'.$v['title'].'</strong><br>'
                    .'<small class="text-muted">'.date('y-m-d',$v['time']).' Author:'. $v['author'].'</small>'
                    .'<div class="well">'.htmlspecialchars(mb_substr($v['content'],0,99,'utf-8'))
                    .'</div>'
                    .'<div class="pull-right">'
                    .getPrez($v['id'],$this->USER['uid'],$v['prez'])
                    .'</div></div></div>';
            }

            $info = array(
                "page" =>$p+1,
                "list" =>$data,
                "status"=>"y",
            );
            $this->ajaxReturn($info,'json');
        }
        $info = array(
            "info"=>"系统君没有找到更多的数据！",
            "status"=>"n",
        );
        $this->ajaxReturn($info,'json');

    }


    //部门成员
    public function department()
    {
        $p = isset($_GET['p'])?intval($_GET['p']):'1';

        $user = M('member');
        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $count = $user->where($where)->count();
        $prefix = C('DB_PREFIX');
        $list  = $user->field("{$prefix}member.*,{$prefix}auth_group.id as gid,{$prefix}auth_group.title,{$prefix}department.name as dept_name")
            ->order("{$prefix}member.t desc")
            ->join("left join {$prefix}auth_group_access ON {$prefix}member.uid = {$prefix}auth_group_access.uid")
            ->join("left join {$prefix}auth_group ON {$prefix}auth_group.id = {$prefix}auth_group_access.group_id")
            ->join("left join {$prefix}department ON {$prefix}department.id = {$prefix}member.dept_id")
            ->limit($offset.','.$pagesize)->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display('department');

    }

    //我的文件公共页面
    public function event(){

        if(IS_POST){
            $data['name'] = isset($_POST['name'])?$_POST['name']:'';
            $data['time'] = time();
            $data['type'] = isset($_POST['type'])?$_POST['type']:'';
            $data['path'] = isset($_POST['path'])?$_POST['path']:'';
            $data['uid'] = $this->USER['uid'];

            $fid = M('document')->data($data)->add();
            if($fid){
                addlog('新增文件，ID：'.$fid);

            }
        }

        $this->display();
    }

    //我的文件
    public function files($p=0)
    {
        $type = isset($_GET['type'])?$_GET['type']:'';
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("参数错误！");
        }
        $where = "uid ='{$uid}'";

        if($type <> ''){
            $where .= " AND type='{$type}'";
        }

        if($keyword <>''){
            $where .= " AND name LIKE '%$keyword%' OR path LIKE '%$keyword%'";
        }

        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $count = M('document')->where($where)->count();
        $list  = M('document')->where($where)->order("fid desc")->limit($offset.','.$pagesize)->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();

        $type = M('document')->field('type')->where("uid=".$uid)->group('type')->select();
        $this->assign('type',$type);
        $this->assign('list',$list);
        $this->assign('page',$page);

        $this->display();

    }

    //个人资料
    public function personal()
    {

        //登录状态下才获取uid
        $uid = isset($this->USER['uid'])?intval($this->USER['uid']):false;

        //用户提交数据后
        if(IS_POST)
        {
            //获取表单提交状态
            $data = I('post.');

            //如果生日字段存在
            if($data['birthday']){
                $data['birthday'] = isset($data['birthday'])?strtotime($data['birthday']):0;
            }

            if(!empty($data['password'])){
                //如果密码相等
                if($data['password'] == $data['repassword'])
                {

                    $data['password'] = password($data['password']);

                }else{
                    $info = array(
                        "info"=>"两次密码输入不一致！",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }

            if($uid)
            {

                    //修改数据
                    M('member')->data($data)->where("uid=$uid")->save();
                    addlog('修改个人资料');
                    $info = array(
                        "info"=>"恭喜,操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');

            }
            //返回错误提示
                $info = array(
                    "info"=>"参数错误！",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');

        }else{
            //获取成员信息
            if($uid){
                $prefix = C('DB_PREFIX');
                $user = M('member');
                $member  = $user->field("{$prefix}member.*,{$prefix}auth_group_access.group_id")->join("{$prefix}auth_group_access ON {$prefix}member.uid = {$prefix}auth_group_access.uid")->where("{$prefix}member.uid=$uid")->find();

            }else{
                $this->ajaxReturn('参数错误！');
            }

            $department = M('department')->field('id,pid,name,time')->order('time asc')->select();
            $tree = new Tree($department);
            $str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
            $department = $tree->get_tree(0,$str,$member['dept_id']);
            $usergroup = M('auth_group')->field('id,title')->select();

            $this->assign('usergroup',$usergroup);
            $this->assign('department',$department);
            $this->assign('member',$member);
            $this->display('personal');

        }

    }
}