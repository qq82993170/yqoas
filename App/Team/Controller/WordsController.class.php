<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：笔记管理控制器
 */
namespace Team\Controller;
use Think\Controller;
use Vendor\Tree;
class WordsController extends ComController
{


    public function index($p=1){

        $source = isset($_GET['source'])?intval($_GET['source']):0;
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("参数错误！");
        }
        $words = M('words');
        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $prefix = C('DB_PREFIX');
        $where = "{$prefix}words.uid=$uid";
        if($source <> ''){
            $where .= " AND {$prefix}words.source=$source";
        }
        if($keyword){
            $where .=" AND {$prefix}words.content LIKE '%$keyword%' OR {$prefix}words.title LIKE '%$keyword%'";
        }
        $count = $words->where($where)->count();
        $list  = $words->where($where)->order("time desc")->limit($offset.','.$pagesize)->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();

        $this->assign('list',$list);
        $this->assign('page',$page);
        $this -> display();
    }

    public function del(){

        $wids = isset($_REQUEST['wid'])?$_REQUEST['wid']:false;
        if($wids){
            if(is_array($wids)){
                $wids = implode(',',$wids);
                $map['wid']  = array('in',$wids);
            }

            $map = 'wid='.$wids;

            if(M('words')->where($map)->delete()){
                addlog('删除笔记，WID：'.$wids);
                $info = array(
                    "info"=>"恭喜,笔记删除成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $info = array(
                    "info"=>"警告！参数错误！。",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }
        }else{
            $info = array(
                "info"=>"警告！参数错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    public function view($wid= 0 ){
        $wid = intval($wid);
        $uid = $this->USER['uid'];
        $words = M('words')->field("words.*,member.name as author")->join("member on member.uid=words.uid",'LEFT')->where("words.wid='{$wid}'")->find();
        //上一篇
        $prev =  M('words')->where("wid < $wid and uid ='{$uid}'")->order('wid desc')->limit('1')->find();
        $this ->assign("prev",$prev);
        //下一篇
        $next =  M('words')->where("wid > $wid and uid ='{$uid}'")->order('wid asc')->limit('1')->find();
        $this ->assign("next",$next);
//        dump($next);
        $this->assign('words',$words);
        $this->display();
    }



    //笔记列表
    public function add(){


        $this -> display();
    }

    public function edit($wid = 0,$stu = 0){

        $wid = intval($wid);
        $stu = intval($stu);
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("状态错误！");
        }
        if($stu){

            $words = M('words')->field("content")->where("wid='{$wid}'")->find();
            echo $words['content'];
//            dump($words);die;
        }
        else{
            $words = M('words')->where("wid='{$wid}'")->find();
            $this -> assign('words',$words);
            $this -> display();
        }

    }

    public function update($wid=0){

        $wid = intval($wid);
        $data['title'] = isset($_POST['title'])?$_POST['title']:false;
        $data['source'] = isset($_POST['source'])?$_POST['source']:0;
        $data['content'] = isset($_POST['content'])?$_POST['content']:false;
        $data['time'] = time();

        $uid = $this->USER['uid'];
        if(!$uid){
            $info = array(
                "info"=>"警告！状态错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }
        else{
            $data['uid'] = $uid;
            $no_read = isset($_POST['no_read'])?$_POST['no_read']:0;
            if($no_read){
                cookie('no_read',$no_read,3600*24*365);//360天不显示，清除cookie可再次显示
            }else{
                cookie('no_read',0);
            }

            if(!$data['title'] or !$data['content']){
                $info = array(
                    "info"=>"警告！笔记标题及笔记内容为必填项目。",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

            if($wid){
                M('words')->data($data)->where('WID='.$wid)->save();
                addlog('编辑笔记，WID：'.$wid);
                $info = array(
                    "info"=>"恭喜！笔记编辑成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }
            else{
                $wid = M('words')->data($data)->add();
                if($wid){
                    addlog('新增笔记，WID：'.$wid);
                    $info = array(
                        "info"=>"恭喜,操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }else{
                    $info = array(
                        "info"=>"抱歉，未知错误！",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }

            }

        }

    }


}