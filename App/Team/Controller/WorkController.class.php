<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：工作管理控制器
 */

namespace Team\Controller;
use Think\Controller;
class WorkController extends ComController
{

    //工作记录
    public function index($p=1,$action='')
    {

        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $options = isset($_GET['options'])?$_GET['options']:'';

        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        if($action == 'dept'){
            $where = "";
        }else{
            $where = "works.uid='{$uid}'";
            if($options){
                $where .=' AND ';
            }
        }
        //获取日期条件
        switch($options){
            case 'day':
                $today = strtotime(date('Y-m-d',time()));
                $where .= "works.date >='{$today}'";
                break;

            case 'week':
                $week = strtotime('last week');
                $where .= "works.date >='{$week}'";
                break;

            case 'month':
                $month = strtotime("-1 month");
                $where .= "works.date >='{$month}'";
                break;

            default:
                $where .= '';
              break;
        }
        //搜索条件
        if($keyword <> '' && $action !== 'dept'){
            $where .=" AND works.content LIKE '%$keyword%'";
        }else{
            if($keyword <> '' && $action == 'dept' && $options<>''){
                $where .=" AND works.content LIKE '%$keyword%'";
            }
        }

        $count = M('works')->where($where)->count();
        $list = M('works')->field('works.*,member.name')
            ->join('member on member.uid=works.uid','left')
            ->where($where)
            ->order('works.date desc')
            ->limit($offset.','.$pagesize)
            ->select();

        $page	=   new \Think\Page($count,$pagesize);
        $page = $page->show();

        $this->assign('list',$list);
        $this->assign('page',$page);

        $this->display();

    }

    //获取工作类型
    public function getSect($pid=0){

        if($pid){
            $data = M('reports')->field('name,id')->where("pid={$pid}")->select();
            $this->ajaxReturn($data,'json');
        }

        echo '参数错误';

    }

    public function del(){

        $id =  I('post.id','','intval');
        $uid = $this->USER['uid'];
        if($uid){

            if(M('works')->where("id={$id} AND uid={$uid}")->delete()){
                addlog('删除工作报表，ID：'.$id);
                $info = array(
                    'info'=>'恭喜，操作成功！',
                    'status'=>'y'
                );
                $this->ajaxReturn($info,'json');
            }else{
                $info = array(
                    'info'=>'哎哟参数错误咯！',
                    'status'=>'n'
                );
                $this->ajaxReturn($info,'json');
            }

        }else{
            echo '参数错误！';die;
        }

    }

    //工作提交
    public function add()
    {

        $sect= M('reports')->field('name,id')->where("pid=0")->select();

        $this->assign('sect',$sect);
        $this->display();

    }

    //编辑
    public function edit($id=0){

        $uid = $this->USER['uid'];
        $data = M('works')->where("id='{$id}' AND uid='{$uid}'")->find();
        $fid3 = $data['rid'];
        $fid2 = getFid($fid3);
        $fid1 = getFid($fid2);

        if(!$fid1){
            $fid1=$fid2;
            $fid2=$fid3;
            $fid3='';
        }
//        dump($fid1);dump($fid2);dump($fid3);

        if($fid1){
                $sect1= M('reports')->field('name,id')->where("pid=0")->select();
                $this->assign('sect1',$sect1);

            }

        if($fid2){
                $sect2= M('reports')->field('name,id')->where("pid=$fid1")->select();
                $this->assign('sect2',$sect2);

            }

        if($fid3){
                $sect3= M('reports')->field('name,id')->where("pid=$fid2")->select();
                $this->assign('sect3',$sect3);
            }

//        dump($sect1);dump($sect2);dump($sect3);
        $this->assign('data',$data);
        $this->assign('fid1',$fid1);
        $this->assign('fid2',$fid2);
        $this->assign('fid3',$fid3);
        $this->display();
    }

    //审核状态修改
    public function saveStu(){

        //获取id
        $id = I('get.id',0,'intval');
        $sid = I('get.sid',0,'intval');
        M('works')->data(array('status'=>$sid))->where("id='$id'")->save();
        die('1');

    }

    //更新数据
    public  function  update(){
        $id = I('post.id','','intval');
        $date = I('post.date','');
        $data['date'] = isset($_POST['date'])?strtotime($_POST['date']):'';
        $data['rid'] =  isset($_POST['rid'])?trim($_POST['rid']):'';
        $data['content'] =  isset($_POST['content'])?trim($_POST['content']):'';
        $data['start'] = isset($_POST['start'])?strtotime($date .' '.$_POST['start']):'';
        $data['end'] = isset($_POST['end'])?strtotime($date .' '.$_POST['end']):'';
        $data['uid'] = $this->USER['uid'];
        $data['status'] =  isset($_POST['status'])?intval($_POST['status']):0;
        if($data['end'] <= $data['start']+1800){
            $info = array(
                "info"=>"操作错误，结束时间必须为开始时间的半小时之后！",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }
        if($id){
            if(M('works')->data($data)->where("id='{$id}'")->save()){
                addlog('修改工作报表，ID：'.$id);
                $info = array(
                    "info"=>"恭喜,操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }
        }else{
            $id =  M('works')->data($data)->add();
            if($id){
                addlog('填写工作报表，ID：'.$id);
                $info = array(
                    "info"=>"恭喜,操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }
        }
        $info = array(
            "info"=>"您的内容无改动,不需要 保存！",
            "status"=>"n",
        );
        $this -> ajaxReturn($info,'json');
    }


}