<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：公司动态管理控制器
 */
namespace Team\Controller;
use Think\Controller;
class NewsController extends ComController
{
    //动态
    public function index($p=0)
    {

        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("参数错误！");
        }
        $where = '';
        if($keyword <>''){
            $where = "dynamic.title LIKE '%$keyword%' OR dynamic.content LIKE '%$keyword%'";
        }

        $pagesize = 15;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $count = M('dynamic')->where($where)->count();
        $list  = M('dynamic')->field('member.name as uname,dynamic.*')
            ->join("member on member.uid=dynamic.uid","left")
            ->where($where)
            ->order("dynamic.id desc")
            ->limit($offset.','.$pagesize)
            ->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();

        $this->assign('list',$list);
        $this->assign('page',$page);

        $this->display();
    }


    public function del(){

        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:false;
        $ids = json_decode($ids,true);
//        dump($ids);die;
        if($ids){
            if(is_array($ids)){
                foreach($ids as $k=>$v){
                    $ids[$k] = intval($v['ids']);
                }
                $ids = implode(',',$ids);
                $map['id']  = array('in',$ids);
                if(M('dynamic')->where($map)->delete()){
                    addlog('删除动态ID：'.$ids);
                    $info = array(
                        "info"=>"恭喜操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }else{
                if(M('dynamic')->where("id={$ids}")->delete()){
                    addlog("删除动态ID：",$ids);
                    $info = array(
                        "info"=>"恭喜操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }
        }
        $info = array(
            "info"=>"警告！参数错误！。",
            "status"=>"n",
        );
        $this -> ajaxReturn($info,'json');

    }


    public function view($sid=''){

        $sid = intval($sid);

        $news = M('dynamic')
            ->field("member.name as author,dynamic.*")
            ->join("member on member.uid=dynamic.uid","left")
            ->where("dynamic.id='{$sid}'")
            ->order("dynamic.id desc")
            ->find();

        //上一篇
        $prev =  M('dynamic')->where("id < $sid")->order('id desc')->limit('1')->find();
        $this ->assign("prev",$prev);
        //下一篇
        $next =  M('dynamic')->where("id > $sid")->order('id asc')->limit('1')->find();
        $this ->assign("next",$next);

        $this->assign('news',$news);
        $this->display();

    }


    public function prez(){

        $id = isset($_REQUEST['id'])?$_REQUEST['id']:false;
        $uid = $this->USER['uid'];
        if($id){

            //更新数据
            if(M('dynamic')->where("id=".$id)->setInc('prez',1)){
                //存入点赞记录到cookies时间为2天 [pid] [uid]
                savePrez($id,$uid);
                $info = array(
                    "info"=>"恭喜操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }
        }
            $info = array(
                "info"=>"警告！参数错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');


    }
}