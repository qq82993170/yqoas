<?php
/**
 * Created by PhpStorm.
 * User: ThinkPad
 * Date: 2016/11/26
 * Time: 19:43
 */
namespace Team\Controller;
use Think\Controller;
class SmsimController extends ComController
{
    //search
    public function search(){

        $uid = $this->USER['uid'];
        $keyword = isset($_POST['keyword'])?$_POST['keyword']:'';
        $where = "uid !={$uid}";
        if($keyword <>''){
            $where .= " AND name LIKE '%$keyword%'";
        }
        $data = M('member')->field('uid,name,head')->where("$where")->order('t desc')->select();
        if($data){
            //数据
            $log =array();
            foreach($data as $k=>$v){
                $log[$k]['id']=$v['uid'];
                $log[$k]['name']=$v['name'];
                $log[$k]['face']=$v['head'];
            }
            //输出json数据
            $info =array(
                'status'=>1,
                'msg'=>'ok',
                'data'=>$log
            );
        }else{
            //输出json数据
            $info =array(
                'status'=>0,
            );
        }

        $this->ajaxReturn($info,'json');

    }

    //好友列表
    public function friend(){

        $uid = $this->USER['uid'];
        $data = M('member')->field('uid,name,head')->where("uid !={$uid}")->order('t desc')->select();
        $user =array();
        foreach($data as $k=>$v){
            $user[$k]['id']=$v['uid'];
            $user[$k]['name']=$v['name'];
            $user[$k]['face']=$v['head'];
        }
        $dept_id = $this->USER['dept_id'];
        $data = M('member')->field('uid,name,head')->where("uid !={$uid} AND dept_id={$dept_id}")->order('t desc')->select();
        $dept =array();
        foreach($data as $k=>$v){
            $dept[$k]['id']=$v['uid'];
            $dept[$k]['name']=$v['name'];
            $dept[$k]['face']=$v['head'];
        }
        //输出json数据
        $info =array(
            'status'=>1,
            'msg'=>'ok',
            'data'=>array(
               array('name'=>'公司成员',
                   'nums'=> count($user),
                   'id'=>1,
                   'item'=>$user
               ),
                array('name'=>'部门成员',
                    'nums'=> count($dept),
                    'id'=>2,
                    'item'=>$dept
                ),
            ),
        );
        $this->ajaxReturn($info,'json');
    }

    //聊天列表
    public function chatlog(){

        $uid = $this->USER['uid'];
        $data = M('comment')->field('comment.accepter,member.name,member.head as face,max(comment.time) as time')
            ->join('member on member.uid=comment.accepter','left')
            ->where("comment.be={$uid} and comment.type=3")
            ->group('comment.accepter')
            ->order('comment.time desc')
            ->select();
        //记录数据
        $log =array();
        foreach($data as $k=>$v){
            $log[$k]['id']=$v['accepter'];
            $log[$k]['name']=$v['name'];
            $log[$k]['time']=beenTime($v['time']);
            $log[$k]['face']=$v['face'];
        }
        //输出json数据
        $info =array(
            'status'=>1,
            'msg'=>'ok',
            'data'=>$log
        );
        $this->ajaxReturn($info,'json');

    }

    //发送消息
    public function update(){
        //获取提交上来的数据
        $data['accepter']= isset($_POST['id'])?$_POST['id']:'';
        $data['author'] = $this->USER['name'];
        $data['content'] = isset($_POST['content'])?$_POST['content']:'';
        $data['time'] = isset($_POST['_'])?strtotime($_POST['_']): time();
        $data['be'] = $this->USER['uid'];
        $data['type']=3;//3为聊天记录
        //判断是否存入
        if(M('comment')->data($data)->add()){
            $info = array(
                'status'=>1,
                'msg'=>'发送成功',
            );
        }else{
            //发送提示信息
            $info = array(
                'status'=>0,
                'msg'=>'发送失败',
            );
        }

        $this->ajaxReturn($info,'json');

    }

    //获取当前用户头像
    public function getFace(){

        $head = $this->USER['head'];
        header("Location:$head");

    }

    //接受消息
    public function accept(){
        $uid = $this->USER['uid'];
        $accepter = isset($_POST['id'])?$_POST['id']:'';
        $type = isset($_POST['type'])?$_POST['type']:0;
        //读取最新消息
        if(!empty($accepter) && $type ==1) {
            $data = M('comment')->field('comment.content,member.name,member.head as face,comment.time')
                ->join('member on member.uid=comment.be', 'left')
                ->where("comment.be={$accepter} and comment.type=3 and comment.accepter={$uid}")
                ->order('comment.time desc')
                ->find();

            if ($_COOKIE['smsim_accept'.$data['name']] !== $data['time']) {
                cookie('smsim_accept'.$data['name'], $data['time'],24*3600);
                //组合success
                $info = array(
                    'status' => 1,
                    'msg' => array(
                        'name' => $data['name'],
                        'time' => beenTime($data['time']),
                        'face' => $data['face'],
                        'content' => $data['content']
                    )
                );
            } else {
                //组合error
                $info = array(
                    'status' => 0,
                    'msg' => ''
                );
            }
        }elseif(!empty($accepter) && $type ==2){
            $today = strtotime(date('Y-m-d',time()));
            $data = M('comment')->field('comment.content,member.name,member.head,comment.time,comment.be')
                ->join('member on member.uid=comment.be', 'left')
                ->where("(comment.be={$accepter} and comment.type=3 and comment.accepter={$uid}) or (comment.be={$uid} and comment.type=3 and comment.accepter={$accepter} ) and comment.time >= $today")
                ->order('comment.time asc')
                ->select();
            cookie('smsim_accept'.$data['name'], $data[count($data)]['time'],24*3600);
            $chactlog = array();
            foreach($data as $k=>$v){
                $chactlog[$k]['id']=$v['be'];
                $chactlog[$k]['name']=$v['name'];
                $chactlog[$k]['time']=beenTime($v['time']);
                $chactlog[$k]['face']=$v['head'];
                $chactlog[$k]['content']=$v['content'];
            }
            //组合success
            $info = array(
                'status' => 1,
                'msg' =>$chactlog
            );
        }else{
            //组合error
            $info = array(
                'status'=>0,
                'msg'=>''
            );
        }



        $this->ajaxReturn($info,'json');
    }

}


