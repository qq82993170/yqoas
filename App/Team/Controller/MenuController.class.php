<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：菜单管理控制器
 */

namespace Team\Controller;
use Think\Controller;
class MenuController extends ComController
{
    //更新日志
    public function index($str='')
    {
        //所有分类数据
        $allmenu = M('auth_rule')->field('id,title,pid,name,icon,mold')->order('o ASC')->select();
        $allmenu = $this->getMenu($allmenu);
        $this->assign('style',$style);
        $this->assign('info',$info);
        $this->assign('allmenu', $allmenu);
        $this->display();
    }

    //删除数据
    public function del(){

        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:false;
        $ids = json_decode($ids,true);

        if(!empty($ids))
        {
            //uid为1的禁止删除
            if(is_array($ids))
            {
                //多维数组转换成一维数组
                $ids= getNewArr($ids);
                foreach($ids as $k=>$v){
                    $ids[$k] = intval($v['id']);
                }

                if(!$ids){
                    $info = array(
                        "info"=>"参数错误",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }
//            dump($ids);die;
            $topMenu  = array(24, 27, 39,18,5,59,99,100);
            foreach($topMenu as $k=>$v){
                $res = in_array($v,$ids);
                if($res){
                    $info = array(
                        "info"=>"禁止删除系统默认菜单：个人中心、部门管理、系统配置,考勤管理,工作管理,顶部导航、顶部右侧导航、页面左侧导航默认顶级类",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }
            }
//            dump($res);die;
            $map['id']  = array('in',$ids);
            if(M('auth_rule')->where($map)->delete()){

                addlog('删除菜单ID：'.$ids);
                $info = array(
                    "info"=>"恭喜操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $info = array(
                    "info"=>"参数错误",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }
        }
        else
        {
            $info = array(
                "info"=>"您还没选择要删除的菜单！",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    //保存调整
    public function save(){

        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:false;
        $ids = json_decode($ids,true);
//        dump($ids);
        if(!empty($ids))
        {

            if(is_array($ids))
            {
                //获取带参菜单一维数组
                $data = menuNewArr($ids);

                if(!$data){
                    $info = array(
                        "info"=>"参数错误",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                    $uids = implode(',',$uids);
                }
            }
//            dump($data);die;
            if(batch_update('auth_rule',$data,'id')){
                $info = array(
                    "info"=>"恭喜操作成功！",
                    "status"=>"y",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $info = array(
                    "info"=>"oh!您还没有调整过菜单嚄！",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

        }
        else {
            $info = array(
                "info"=>"您还没有添加菜单！",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    //数据编辑
    public function edit($id=0){

        $id = intval($id);
        $m = M('auth_rule');
        $currentmenu = $m->where("id='$id'")->find();
        if(!$currentmenu) {
            $data = array(
                'info'=>'读取数据失败！',
                'status'=>'n',
            );
            $this->ajaxReturn($data,'json');
        }

        $data = array(
            'info'=>'读取数据成功！',
            'currentmenu'=>$currentmenu,
            'status'=>'y',
        );
        $this->ajaxReturn($data,'json');


    }

    //数据添加
    public function update(){
        $id = I('post.id','','intval');
        $data['title'] = I('post.title','','strip_tags');
        $data['name'] = I('post.name','','strip_tags');
        $data['icon'] = I('post.icon');
        $data['islink'] = I('post.islink','','intval');
        $data['status'] = 1;
        $data['mold'] = I('post.mold');
        $data['o'] = I('post.o','','intval');
        $data['tips'] = I('post.tips');
        if($id){
            M('auth_rule')->data($data)->where("id='{$id}'")->save();
            addlog('编辑菜单，ID：'.$id);
        }else{
            M('auth_rule')->data($data)->add();
            addlog('新增菜单，名称：'.$data['title']);
        }
        $info = array(
            "info"=>"恭喜操作成功！",
            "status"=>"y",
        );
        $this -> ajaxReturn($info,'json');
    }

}