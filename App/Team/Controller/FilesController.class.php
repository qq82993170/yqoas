<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：公司文件管理控制器
 */
namespace Team\Controller;
use Think\Controller;
class FilesController extends ComController
{
    //文件列表
    public function index($p=0)
    {
        $type = isset($_GET['type'])?$_GET['type']:'';
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $p = intval($p)>0?$p:1;
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("参数错误！");
        }
        if($type <> ''){
            $where = "document.type='{$type}'";
        }

        if($keyword <>''){
            $where = "document.name LIKE '%$keyword%' OR document.path LIKE '%$keyword%'";
        }

        $pagesize = 20;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $count = M('document')->where($where)->count();
        $list  = M('document')->field('member.name as uname,document.*')
            ->join("member on member.uid=document.uid",'left')
            ->where($where)
            ->order("document.fid desc")
            ->limit($offset.','.$pagesize)
            ->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();

        $type = M('document')->field('type')->group('type')->select();
        $this->assign('type',$type);
        $this->assign('list',$list);
        $this->assign('page',$page);

        $this->display();
    }



}