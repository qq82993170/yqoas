<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：考勤管理控制器
 */

namespace Team\Controller;
use Think\Controller;
use Vendor\Tree;
class AttenceController extends ComController
{
    //绩效考评
    public function index()
    {

        $month = isset($_GET['month'])?strtotime($_GET['month']):strtotime(date('Y-m',time()));
        $where ="monthly.date ={$month}";
        $type = $this->USER['type'];
        //如果是经理则获取同部门下的成员月报列表
        if($type == 3){
            $dptid = $this->USER['dept_id'];
            $where .=" AND member.dept_id={$dptid}" ;
        }
        $list = M('monthly')->field('monthly.*,member.name')
            ->join('member on member.uid=monthly.uid','left')
            ->where($where)
            ->order('monthly.date desc')
            ->select();
        $this->assign('list',$list);
        $this->assign('month',$month);

        $this->display();

    }

    //工作报表统计
    public function works()
    {
        //本月时间戳
        $month = isset($_GET['month'])?strtotime($_GET['month']):strtotime(date('Y-m',time()));
        $uid = isset($_GET['uid'])?intval($_GET['uid']):'';
        $rid = isset($_GET['rid'])?intval($_GET['rid']):'';
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $where ="works.date >={$month} AND works.status =1";
        $type = $this->USER['type'];
        //如果是普通员工则控制页面不能访问！
        if($type == 0){
            echo '无权访问当前页面！';
            die;
        }
        if($uid){
            $where .= " AND works.uid='{$uid}'";
        }
        if($rid){
            $where .= " AND works.rid='{$rid}'";
        }
        if($keyword <> ''){
            $where .= " AND works.content LIKE '%$keyword%'";
        }
        //获取成员
        $members = M('member')->field('uid,name,user')->order('uid asc')->select();
        $this->assign('members',$members);
        //获取类型
        $reports = M('reports')->field('id,pid,name')->order('o asc')->select();
        $tree = new Tree($reports);
        $str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
        $reports = $tree->get_tree(0,$str,$rid);
        $this->assign('reports',$reports);
        //获取列表
        $list = M('works')->field('works.*,member.name')
            ->join('member on member.uid=works.uid','left')
            ->where($where)
            ->order('works.date desc')
            ->select();
        $this->assign('list',$list);
        $this->assign('month',$month);

        $this->display();
    }

    //工作审核列表
    public function reports()
    {
        //今天之前的数据
        $month = isset($_GET['month'])?strtotime($_GET['month']):strtotime(date('Y-m-d',time()));
        $where ="works.date <={$month} AND works.status =0";
        $type = $this->USER['type'];
        //如果是经理则获取同部门下的成员月报列表
        if($type == 3){
            $dptid = $this->USER['dept_id'];
            $where .=" AND member.dept_id={$dptid}" ;
        }
        $list = M('works')->field('works.*,member.name')
            ->join('member on member.uid=works.uid','left')
            ->where($where)
            ->order('works.date desc')
            ->select();
        $this->assign('list',$list);
        $this->assign('month',$month);

        $this->display();

    }



}