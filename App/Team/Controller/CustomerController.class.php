<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：客户管理控制器
 */
namespace Team\Controller;
use Think\Controller;
class CustomerController extends ComController
{
    //客户列表
    public function index()
    {

        $p = isset($_GET['p'])?intval($_GET['p']):'1';
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $uid = $this->USER['uid'];
        $where = 'uid='.$uid;
        $prefix = C('DB_PREFIX');
        if($keyword <>''){
            $where .=" AND {$prefix}hightech LIKE '%$keyword%' OR {$prefix}email LIKE '%$keyword%' OR {$prefix}qq LIKE '%$keyword%' OR {$prefix}phone LIKE '%$keyword%' OR {$prefix}description LIKE '%$keyword%' OR {$prefix}name LIKE '%$keyword%' OR {$prefix}tel LIKE '%$keyword%'";
        }

        $pagesize = 50;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $count = M('customer')->where($where)->count();
        $list  = M('customer')->where($where)->order('time desc')->limit($offset.','.$pagesize)->select();
        $info['count'] = $count;
        $info['time'] = $list[0]['time'];
        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->assign('info',$info);
        $this -> display();

    }

    //编辑
    public function add(){

        $this->display();
    }

    //编辑
    public function edit($id=0){

        $id = intval($id);
        $uid = $this->USER['uid'];
        if(!$uid){
            $this->ajaxReturn("状态错误！");
        }
        $customer = M('customer')->where("id='{$id}'")->find();
        $this -> assign('customer',$customer);
        $this -> display();

    }

    //删除
    public function del(){

        $id= isset($_REQUEST['id'])?$_REQUEST['id']:false;

        if($id){
                //删除客户信息
                if(M('customer')->where("id=".$id)->delete()){
                    addlog('删除客户信息ID：'.$id);
                    $info = array(
                        "info"=>"恭喜操作成功！",
                        "status"=>"y",
                    );
                    $this -> ajaxReturn($info,'json');
                }else{
                    $info = array(
                        "info"=>"参数错误",
                        "status"=>"n",
                    );
                    $this -> ajaxReturn($info,'json');
                }

        }else{
            $info = array(
                "info"=>"警告！参数错误！。",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    public function view($id=0){

        $id = intval($id);
        $uid = $this->USER['uid'];
        $customer = M('customer')
            ->field("customer.*,member.name as author")
            ->join("member on member.uid=customer.uid",'LEFT')
            ->where("customer.id <='{$id}' and customer.uid ='{$uid}'")
            ->order('id desc')
            ->limit(10)
            ->select();
        $this->assign('customer',$customer);

        $this->display();

    }

    public function update($ajax=''){
        if($ajax=='yes'){
            $id = I('get.id',0,'intval');
            $stu = I('get.stu',0,'intval');
            if($id !==1){
                M('customer')->data(array('stutas'=>$stu))->where("id='$id'")->save();
                die('1');
            }
        }
        $id = isset($_POST['id'])?intval($_POST['id']):false;
        $head = I('post.head','','strip_tags');
        $data['sex'] = isset($_POST['sex'])?intval($_POST['sex']):0;
        $data['head'] = $head?$head:'';
        $data['phone'] = isset($_POST['phone'])?$_POST['phone']:'';
        $data['qq'] = isset($_POST['qq'])?trim($_POST['qq']):'';
        $data['email'] = isset($_POST['email'])?trim($_POST['email']):'';
        $data['tel'] = isset($_POST['tel'])?trim($_POST['tel']):'';
        $data['type'] = isset($_POST['type'])?intval($_POST['type']):0;
        $data['description'] = isset($_POST['description'])?$_POST['description']:0;
        $data['address'] = isset($_POST['address'])?$_POST['address']:'';
        $data['hightech'] = isset($_POST['hightech'])?$_POST['hightech']:'';
        $data['time'] = time();//获得时间戳
        $data['uid'] = $this->USER['uid'];//获取用户uid
        $data['name'] = isset($_POST['name'])?$_POST['name']:'';//客户姓名
        $data['stutas'] = isset($_POST['stutas'])?intval($_POST['stutas']):0;//状态录入
//        dump($data);die;
        if(!$id){
            if($data['name']==''){
                $info = array(
                    "info"=>"客户姓名不能为空！",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

            if(M('customer')->where("name='{$data['name']}'")->count()){
                $info = array(
                    "info"=>"客户名已被占用！",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }

            $id = M('customer')->data($data)->add();
            addlog('新增客户，客户id：'.$id);
        }else{
            M('customer')->data($data)->where("id=$id")->save();
            addlog('编辑客户信息，客户id：'.$id);
        }
        $info = array(
            "info"=>"操作成功！",
            "status"=>"n",
        );
        $this -> ajaxReturn($info,'json');
    }

}