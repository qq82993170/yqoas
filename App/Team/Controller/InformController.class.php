<?php
/**
 *
 * 功能说明：通告控制器。
 *
 **/
namespace Team\Controller;
use Think\Controller;
use Vendor\Tree;
class InformController extends ComController {
    public function index(){

        $p = isset($_GET['p'])?intval($_GET['p']):'1';
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $where = '';

        if($keyword <>''){
            $where ="content LIKE '%$keyword%' OR title LIKE '%$keyword%'";
        }

        $pagesize = 10;#每页数量
        $offset = $pagesize*($p-1);//计算记录偏移量
        $count =  M('inform')->where($where)->count();

        $list  = M('inform')
            ->order("{$prefix}inform.id desc")
            ->where($where)
            ->limit($offset.','.$pagesize)->select();

        $page	=	new \Think\Page($count,$pagesize);
        $page = $page->show();
        $this->assign('list',$list);
        $this->assign('page',$page);

        $this -> display();

    }

    public function del(){

        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:false;
        $ids = json_decode($ids,true);
        if(is_array($ids))
        {
            foreach($ids as $k=>$v){
                $ids[$k] = intval($v['ids']);
            }

            $ids = implode(',',$ids);

        }

        $map['id']  = array('in',$ids);
        if(M('inform')->where($map)->delete()){
            addlog('删除通告id：'.$ids);
            $info = array(
                "info"=>"恭喜,操作成功！",
                "status"=>"y",
            );
            $this -> ajaxReturn($info,'json');
        }
            $info = array(
                "info"=>"参数错误",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');

    }

    public function edit(){

        $id = isset($_GET['id'])?intval($_GET['id']):false;
        if($id){
            $prefix = C('DB_PREFIX');
            $inform  = M('inform')->where("{$prefix}inform.id='{$id}'")->find();
        }else{
            $this->ajaxReturn('参数错误！');
        }

        $this->assign('inform',$inform);
        $this -> display();
    }

    public function add(){

        $this -> display();
    }

    public function update(){

        $id = isset($_POST['id'])?intval($_POST['id']):false;
        $data['title'] = isset($_POST['title'])?trim($_POST['title']):'';
        $data['start'] = time();
        $data['end'] = isset($_POST['end'])?strtotime(trim($_POST['end'])):'';
        $data['content'] = isset($_POST['content'])?trim($_POST['content']):'';

        if(!$data['end']){
            $info = array(
                "info"=>"结束日期不能为空",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

        if(!$id){

            $id = M('inform')->data($data)->add();
            addlog('新增通告，通告id：'.$id);
        }else{
            M('inform')->data($data)->where("id=$id")->save();
            addlog('编辑通告信息，通告id：'.$id);
        }
        $info = array(
            "info"=>"操作成功！",
            "status"=>"y",
        );
        $this -> ajaxReturn($info,'json');
    }

    public function view($id=''){

        $id = intval($id);

        $inform = M('inform')->where("id='{$id}'")->order("id desc")->find();

        //上一篇
        $prev =  M('inform')->where("id < $id")->order('id desc')->limit('1')->find();
        $this ->assign("prev",$prev);
        //下一篇
        $next =  M('inform')->where("id > $id")->order('id asc')->limit('1')->find();
        $this ->assign("next",$next);

        $this->assign('inform',$inform);
        $this->display();

    }
}

