<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：系统设置控制器
 */

namespace Team\Controller;
use Think\Controller;
class SettingController extends ComController
{
    //更新日志
    public function index()
    {

        $this->display();
    }

    //更新配置
    public function update()
    {

        if(IS_POST){
            $data = $_POST;
            $data['sitename'] = isset($_POST['sitename'])?strip_tags($_POST['sitename']):'';
            $data['title'] = isset($_POST['title'])?strip_tags($_POST['title']):'';
            $data['keywords'] = isset($_POST['keywords'])?strip_tags($_POST['keywords']):'';
            $data['description']= isset($_POST['description'])?strip_tags($_POST['description']):'';
            $data['footer'] = isset($_POST['footer'])?$_POST['footer']:'';
            $Model = M('setting');
            foreach($data as $k=>$v){
                $Model->data(array('v'=>$v))->where("k='{$k}'")->save();
            }
            addlog('修改网站设置。');
            $info = array(
                "info"=>"恭喜网站设置保存成功！",
                "status"=>"y",
            );
            $this -> ajaxReturn($info,'json');
        }

        $this->display('update');
    }



}