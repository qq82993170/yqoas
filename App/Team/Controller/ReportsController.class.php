<?php
/**
 * 创建者 admin.
 * 日期: 2016/10/19
 * 时间: 15:37
 * 描述：报表类型管理控制器
 */

namespace Team\Controller;
use Think\Controller;
use Vendor\Tree;
class ReportsController extends ComController
{
    //类型列表
    public function index(){

        $reports = M('reports')->field('id,pid,name,time')->order('o asc')->select();
        $reports = $this->getMenu($reports);
        $this->assign('reports',$reports);
        $this -> display();
    }

    public function del(){

        $id = isset($_POST['id'])?intval($_POST['id']):false;
        if($id){
            $data['id'] = $id;
            $reports = M('reports');
            $member = M('member');
            if($reports->where('pid='.$id)->count() || $member->where('dept_id='.$id)->count()){
                $info = array(
                    "info"=>"删除失败，有子类型或是有用户存在当前类型下",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                $reports->where('id='.$id)->delete();
                addlog('删除类型，ID：'.$id);
            }
            $info = array(
                "info"=>"恭喜，操作成功！",
                "status"=>"y",
            );
            $this -> ajaxReturn($info,'json');
        }else{
            $info = array(
                "info"=>"删除失败，可能是程序未获取到参数",
                "status"=>"n",
            );
            $this -> ajaxReturn($info,'json');
        }

    }

    //添加界面
    public function add(){

        $pid = isset($_GET['pid'])?intval($_GET['pid']):0;
        $reports = M('reports')->field('id,pid,name,time')->order('time asc')->select();
        $tree = new Tree($reports);
        $str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
        $reports = $tree->get_tree(0,$str, $pid);

        $this->assign('reports',$reports);
        $this -> display();
    }



    //编辑界面
    public function edit($id=0)
    {
        $id = intval($id);
        $m = M('reports');
        $data = $m->where("id='$id'")->find();
        $reports = M('reports')->field('id,pid,name')->order('time asc')->select();
        $tree = new Tree($reports);
        $str = "<option value=\$id \$selected>\$spacer\$name</option>"; //生成的形式
        $reports = $tree->get_tree(0,$str, $data['pid']);

        $this->assign('reports',$reports);
        $this->assign('data',$data);
        $this->display('edit');
    }

    //数据添加
    public function update(){
        $id = I('post.id','','intval');
        $data['name'] = I('post.name','','strip_tags');
        $data['pid'] =  isset($_POST['pid'])?trim($_POST['pid']):'';
        $data['content'] =  isset($_POST['content'])?trim($_POST['content']):'';
        $data['o'] =  isset($_POST['o'])?intval($_POST['o']):0;
        if($id){
            if($data['pid']==$id){
                $info = array(
                    "info"=>"父级类型不可以是当前类型",
                    "status"=>"n",
                );
                $this -> ajaxReturn($info,'json');
            }else{
                M('reports')->data($data)->where("id='{$id}'")->save();
                addlog('编辑类型，ID：'.$id);
            }
        }else{
            $data['time'] =  time();
            M('reports')->data($data)->add();
            addlog('新增类型，名称：'.$data['name']);
        }
        $info = array(
            "info"=>"恭喜,操作成功！",
            "status"=>"y",
        );
        $this -> ajaxReturn($info,'json');
    }

}