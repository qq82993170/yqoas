### 有穹开源OA办公系统介绍

一款有即时通讯功能的多功能内网、外网办公管理系统，方便企业利用及二次开发。


### 手动下载及安装 

使用git下载

```
git clone https://gitee.com/youqiongwang/yqoas
```
配置伪静态 


```
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}
```

配置数据库


```
<?php
return array(
    //数据库链接配置
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => '127.0.0.1', // 服务器地址
    'DB_NAME'   => '', // 数据库名
    'DB_USER'  => '',// 数据库用户名
    'DB_PWD'  => '',// 数据库密码
    'DB_PORT'   => 3306, // 端口
    'DB_PREFIX' => '', // 数据库表前缀
    'DB_CHARSET'=>  'utf8',      // 数据库编码默认采用utf8
);
```

利用mysql工具导入sql文件



### 宝塔自动安装


步骤：登录宝塔面板 -> 软件商店 -> 一键部署 -> 配置域名 -> 搭建成功

### 教程地址

[宝塔版一键部署安装教程](https://www.jianshu.com/p/afc56cca846a)

[系统管理员密码修改教程](https://www.jianshu.com/p/42e545ae8ea9)

### 系统截图

控制台

![控制台](https://images.gitee.com/uploads/images/2019/1111/114604_3096ac4f_1294921.png "oa-1.png")

工作管理

![工作管理](https://images.gitee.com/uploads/images/2019/1111/114622_3f4584ae_1294921.png "oa-2.png")

考勤管理

![考勤管理](https://images.gitee.com/uploads/images/2019/1111/114650_729c0f67_1294921.png "oa-3.png")

系统设置

![系统设置](https://images.gitee.com/uploads/images/2019/1111/114711_5714af9a_1294921.png "oa-4.png")

部门管理

![部门管理](https://images.gitee.com/uploads/images/2019/1111/114738_ee26b897_1294921.png "oa-5.png")

员工空间

![员工空间](https://images.gitee.com/uploads/images/2019/1111/114756_80665519_1294921.png "oa-6.png")

代办功能

![代办功能](https://images.gitee.com/uploads/images/2019/1111/114812_5ce38521_1294921.png "oa-7.png")


### 当前涵盖功能

1.员工空间功能

2.文件存储功能

3.绩效考评功能

4.即时通讯功能  js形式已下架 新版本将会推出sockect形式

5.每日待办功能

6.客户管理功能

7.部门管理功能

8.个人笔记功能

9.内部文件笔记分享功能

10.操作信息记录

11.月报管理功能

12.笔记共享功能

13.员工空间动态发短说说

14.内网传文件功能

15.客户资源管理及分配

16.任务管理功能

18.工作分配功能

19.企业知识管理

更多功能安装探索，其他实用功能正在开发中.....


### 程序配置文件说明

App --- 应用目录

Public --- 公共文件目录

System --- 框架目录

Uploads --- 上传文件目录

index.php --- 入口文件

business.sql --- 数据库文件


### ThinkPHP框架修改的文件说明

ThinkPhp 框架目录 => System  仅仅修改了文件目录名称

ThinkPhp/start.php 框架入口文件 =>  System.php 仅仅修改了文件目录名称 

index.php 框架入口文件 修改了框架入口文件的路径

备注：除了文件名称和目录修改过以外，其他部分代码均未变动，基本都是框架原生书写的代码。

说明：修改文件目录名称是为了防止目录北探测产生一些安全问题；


### 配置目录

App/Common/Conf/db.php ---数据库配置文件

App/Common/Conf/conf.php --- 全局配置文件

App/Common/Conf/debug.php --- 调试工具配置文件

App/Common/Conf/path.php  --- 伪静态配置

App/Common/Conf/route.php  --- 路由配置


### 初始管理员密码

GIT社区开发者开源版
用户名：admin
密码：admin
宝塔面板一键部署版本
用户名：admin
密码：admin888

注：系统默认密码过于简单，用户务必请自行修改成安全密码。

### 安装可能会遇到的一些问题

首先项目是开启了伪静态的，需要执行配置伪静态，nginx 是rewrite  apache 是 .htaccess  使用thinkphp3.2.3 的伪静态配置规则即可，另外项目非自动安装，如需自动安装可使用宝塔进行安装。手动安装需手动配置db.php以及手动导入数据库，进行配置。

### 支持我们

开源版本大家随便用，可保留版权信息对我们鼓励！

拟定后续的付费版发布地址：https://www.chtml.cn/product/show/3073 欢迎支持。

### 其他

项目互助QQ群：834376974，欢迎加群交流。

### 关于bug说明

1.图片不显示的问题，这是因为git没有上传upload文件导致的，可自行替换掉相关图片即可。

2.用户分组不能正常访问，提示json字符串，小白认知【乱码的情况】，这种是没有给到分组权限的缘故，可以通过管理员账户进行授权即可。

### 关于新版本后续的升级说明

1.宝塔面板，直接点击更新即可

2.git版本需手动进行升级，建议直接使用宝塔面板版本。